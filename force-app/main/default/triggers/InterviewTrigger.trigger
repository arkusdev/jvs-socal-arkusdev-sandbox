//**

// Author: Mazuryk Yaroslav
// Date: December 13, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

trigger InterviewTrigger on Interview__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	DisableTrigger__c disableTrigger = [select id,InterviewTrigger__c from DisableTrigger__c];

	if (disableTrigger.InterviewTrigger__c == true){
		InterviewTriggerHandler.entry(new TriggerParams(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete,
        	trigger.isUndelete, trigger.new, trigger.old, trigger.newMap, trigger.oldMap));
	}
}