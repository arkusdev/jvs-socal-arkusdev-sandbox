/*
Trigger Name: ApplicationTriggerForEmail
Object Name: Application
Condition to Run: When ever the Application is Edited
Handler Class Name: ApplicationTriggerForEmailHandler
Description: This trigger will call the apex class when ever the Application Records are Edited 
				and will call the ApplicationTriggerForEmailHandler.sendInvitationEmailTOApplicant
*/
trigger ApplicationTriggerForEmail on Application__c (after update) {
	// DisableTrigger__c disableTrigger = [select id,ApplicationInterviewTrigger__c from DisableTrigger__c];

  //   if (disableTrigger.ApplicationInterviewTrigger__c){
	//     if(trigger.IsAfter && trigger.IsUpdate){
	//     	ApplicationTriggerForEmailHandler.sendInvitationEmailTOApplicant(trigger.new,trigger.oldMap);
	//     }
  //   }
}