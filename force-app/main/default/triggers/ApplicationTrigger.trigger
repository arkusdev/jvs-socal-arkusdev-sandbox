//**

// Author: Mazuryk Yaroslav
// Date: December 12, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

trigger ApplicationTrigger on Application__c (before insert, after insert, before update, after update, after delete) {
	DisableTrigger__c disableTrigger = [select id,ApplicationTrigger__c from DisableTrigger__c];

	if (disableTrigger.ApplicationTrigger__c){
		ApplicationTriggerHandler.entry(new TriggerParams(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete,
				trigger.isUndelete, trigger.new, trigger.old, trigger.newMap, trigger.oldMap));
	}


}