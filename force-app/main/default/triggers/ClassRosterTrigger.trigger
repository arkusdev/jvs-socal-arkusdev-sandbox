//**
// Author: Mazuryk Yaroslav
// Date: January 11, 2019
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

trigger ClassRosterTrigger on Class_Roster__c (before insert, after insert, before update, after update, after delete) {
    DisableTrigger__c disableTrigger = [select id,ClassRosterTrigger__c from DisableTrigger__c];

    if (disableTrigger.ClassRosterTrigger__c){
			System.debug(disableTrigger.ClassRosterTrigger__c);
		ClassRosterTriggerHandler.entry(new TriggerParams(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, 
        	trigger.isUndelete, trigger.new, trigger.old, trigger.newMap, trigger.oldMap));
	}

}