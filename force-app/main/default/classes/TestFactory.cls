//**

// Author: Mazuryk Yaroslav
// Date: December 12, 2018
// Description: <<this code for creting test data for testing>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

@isTest
public class TestFactory {
	/**
	* https://salesforce.stackexchange.com/questions/21137/creating-unit-tests-without-interacting-with-the-database-creating-fake-ids
	* @example
	* Account a = new Account(Id=TestUtility.getFakeId(Account.SObjectType));
	* Opportunity o = new Opportunity(AccountId=a.Id);
	*/
	static Integer s_num = 1;

	public static String getFakeId(Schema.SObjectType sot) {
		String result = String.valueOf(s_num++);
		return sot.getDescribe().getKeyPrefix() + '0'.repeat(12-result.length()) + result;
	}

	public static SObject createSObject(SObject sObj) {
		// Check what type of object we are creating and add any defaults that are needed.
		String objectName = String.valueOf(sObj.getSObjectType());
		// Construct the default values class. Salesforce doesn't allow '__' in class names
		String defaultClassName = 'TestFactory.' + objectName.replaceAll('__(c|C)$|__', '') + 'Defaults';
		// If there is a class that exists for the default values, then use them
		if (Type.forName(defaultClassName) != null) {
			sObj = createSObject(sObj, defaultClassName);
		}
		return sObj;
	}

	public static SObject createSObject(SObject sObj, Boolean doInsert) {
		SObject retObject = createSObject(sObj);
		if (doInsert) {
			insert retObject;
		}
		return retObject;
	}

	public static SObject createSObject(SObject sObj, String defaultClassName) {
	// Create an instance of the defaults class so we can get the Map of field defaults
	Type t = Type.forName(defaultClassName);
	if (t == null) {
	Throw new TestFactoryException('Invalid defaults class.');
	}
	FieldDefaults defaults = (FieldDefaults)t.newInstance();
		addFieldDefaults(sObj, defaults.getFieldDefaults());
	return sObj;
	}

	public static SObject createSObject(SObject sObj, String defaultClassName, Boolean doInsert) {
		SObject retObject = createSObject(sObj, defaultClassName);
	if (doInsert) {
		insert retObject;
	}
		return retObject;
	}

	public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects) {
		return createSObjectList(sObj, numberOfObjects, (String)null);
	}

	public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, Boolean doInsert) {
		SObject[] retList = createSObjectList(sObj, numberOfObjects, (String)null);
	if (doInsert) {
		insert retList;
	}
		return retList;
	}

	public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, String defaultClassName, Boolean doInsert) {
		SObject[] retList = createSObjectList(sObj, numberOfObjects, defaultClassName);
	if (doInsert) {
		insert retList;
	}
		return retList;
	}

	public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects, String defaultClassName) {
		SObject[] sObjs = new SObject[] {};
		SObject newObj;
		Boolean nameIsAutoNumber;

	// Get one copy of the object
	if (defaultClassName == null) {
		newObj = createSObject(sObj);
	} else {
		newObj = createSObject(sObj, defaultClassName);
	}

	// Get the name field for the object
		String nameField = nameFieldMap.get(String.valueOf(sObj.getSObjectType()));
	if (nameField == null) {
		nameField = 'Name';
	}

	// Clone the object the number of times requested. Increment the name field so each record is unique
	for (Integer i = 0; i < numberOfObjects; i++) {
		SObject clonedSObj = newObj.clone(false, true);
	if (!nameIsAutoNumber){
		clonedSObj.put(nameField, (String)clonedSObj.get(nameField) + ' ' + i);
	}
		sObjs.add(clonedSObj);
	}
		return sObjs;
	}

	private static void addFieldDefaults(SObject sObj, Map<Schema.SObjectField, Object> defaults) {
	// Loop through the map of fields and if they weren't specifically assigned, fill them.
		Map<String, Object> populatedFields = sObj.getPopulatedFieldsAsMap();
			for (Schema.SObjectField field : defaults.keySet()) {
			if (!populatedFields.containsKey(String.valueOf(field))) {
			sObj.put(field, defaults.get(field));
			}
		}
	}

	// When we create a list of SObjects, we need to
	private static Map<String, String> nameFieldMap = new Map<String, String> {
		'Account' => 'Name',
		'Contact' => 'LastName',
		'Case' => 'Subject'
            
		};

	public class TestFactoryException extends Exception {}

	// Use the FieldDefaults interface to set up values you want to default in for all objects.
	public interface FieldDefaults {
		Map<Schema.SObjectField, Object> getFieldDefaults();
	}

// To specify defaults for objects, use the naming convention [ObjectName]Defaults.
// For custom objects, omit the __c from the Object Name

	public class AccountDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
				return new Map<Schema.SObjectField, Object> {
				Account.Name => 'Test Account'
			};
		}
	}

	public class ContactDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
				return new Map<Schema.SObjectField, Object> {
				Contact.FirstName 			=> 'First',
				Contact.Email 				=> 'zslaykz@gmail.com',
				Contact.LastName 	        => 'Last',
				Contact.Rank__c             => 'E-1',
                Contact.Era_of_service__c   => 'Unknown',
                Contact.Phone               => '+380668788535',
                Contact.Intake_date__c 		=> Date.newInstance(2016, 2, 17),
                Contact.Birthdate     		=> Date.newInstance(1995, 2, 17)
			};
		}
	}
    
   public class InterviewDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
				return new Map<Schema.SObjectField, Object> {
				Interview__c.Full_Time__c   => true,
				Interview__c.Job_Ready__c 	=> true
			};
		}
	}

	// For run triggers we need to insert two fields with checkbox = true
	public class DisableTriggerDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
				return new Map<Schema.SObjectField, Object> {
				DisableTrigger__c.ApplicationTrigger__c         => true,
    			DisableTrigger__c.InterviewTrigger__c           => true,
    			DisableTrigger__c.ProgramEnrollmentTrigger__c   => true,
				DisableTrigger__c.ClassRosterTrigger__c         => true
			};
		}
	}

	public class EmailTemplateDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
				return new Map<Schema.SObjectField, Object> {
				EmailTemplate.DeveloperName  => 'testtempalte',
				EmailTemplate.isActive       => true,
				EmailTemplate.FolderId       => '00De0000005FAB1EAO',
				EmailTemplate.Name           => 'test',
    			EmailTemplate.TemplateType   => 'text',
    			EmailTemplate.Subject        => 'Your Subject Here'
			};
		}
	}

	//create test object application with requred fields 
	public class ApplicationDefaults implements FieldDefaults{
        public Map<Schema.SObjectField, Object> getFieldDefaults(){
            return new Map<Schema.SObjectField, Object>{
               // Application__c.RecordTypeId => ObjectUtils.getRecordTypeIdsByDeveloperName(Application__c.sObjectType).get('Main'),
                Application__c.FirstName__c       		=> 'First',                
                Application__c.LastName__c        		=> 'Last',
                Application__c.Cell_Phone__c      		=> '+380668788535',
                Application__c.Email__c           		=> 'zslaykz@gmail.com',
                Application__c.Secondary_Email__c 		=> 'zslaykzr@gmail.com',
                Application__c.Other_Phone__c     		=> '+380668788535',
                Application__c.answer1__c         		=> 'Facebook',
                Application__c.answer2__c         		=> 'San Fernando Valley area',
                Application__c.answer3__c         		=> 'Yes',
                Application__c.answer4__c         		=> 'No',
                Application__c.answer5__c         		=> 'No',
                Application__c.answer6__c         		=> 'No',
                Application__c.answer7__c         		=> 'No',
                Application__c.answer8__c       		=> 'No',
                Application__c.State__c        		   	=> 'Alabama',
                Application__c.City__c           		=> 'Lviv',
                Application__c.ZIP_Code__c        		=> '55454',
                Application__c.Street__c         		=> 'Lazarenka 42',               
                Application__c.Company_Name__c    		=> 'red tag',
                Application__c.Job_Title__c       		=> 'developer',
                Application__c.Start_Date__c      		=> Date.newInstance(2016, 2, 17),
                Application__c.End_date__c        		=> Date.newInstance(2018, 2, 17),
                Application__c.Date_of_Application__c 	=> DateTime.Now()
            };
        }
    }
    
    	//create test object class with requred fields 
	public class ClassDefaults implements FieldDefaults{
        public Map<Schema.SObjectField, Object> getFieldDefaults(){
            return new Map<Schema.SObjectField, Object>{
                              
                Class__c.Monday__c           => true,
                Class__c.Tuesday__c          => true,
                Class__c.Wednesday__c        => true,
                Class__c.Thursday__c         => true,
                Class__c.Friday__c           => true,
				Class__c.Status__c           => 'In Progress',
				Class__c.Start_Date__c       => Date.today(),
				Class__c.Auto_Create_Attendance__c => true

            };
        }
    }
    
    //create test object class roster with requred fields 
    public class Class_RosterDefaults implements FieldDefaults{
        public Map<Schema.SObjectField, Object> getFieldDefaults(){
            return new Map<Schema.SObjectField, Object>{               
                //Class_Roster__c.State__c     			=> 'Alabama',
                //Class_Roster__c.City__c      			=> 'lviv',
				Class_Roster__c.Status__c               => 'Training',
                Class_Roster__c.Enroll_in_the_Course__c => true,
                Class_Roster__c.Start_Date__c           => Date.newInstance(2050, 2, 17),
                Class_Roster__c.End_Date__c             => Date.newInstance(2051, 2, 17)
                    
            };
        }
    }
    
        //create test object class roster with requred fields 
    public class AttendanceDefaults implements FieldDefaults{
        public Map<Schema.SObjectField, Object> getFieldDefaults(){
            return new Map<Schema.SObjectField, Object>{
                Attendance__c.Status__c   => 'Absent',                
                Attendance__c.Date__c     =>  Date.today(),
                Attendance__c.Time__c     => Datetime.now()
            };
        }
    }
    
    public class EventsDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults(){
            return new Map<Schema.SObjectField, Object>{
                Events__c.Name  => 'Event'          
            };
        }
    }

    public class ProgramDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                Program__c.Name  		=> 'Program',
                Program__c.Status__c 	=> 'Open'        
            };
        }
    }

    public class ProgramEnrollmentDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                //Program_Enrollment__c.Status__c 	=> 'Enrolled',
                Program_Enrollment__c.Start_Date__c => Date.today()   
            };
        }
    }


    //create base data for testing 
    public static void createApplicationBaseTestData(){
    	Id RecordTypeId = ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get('Applicant');
    	DisableTrigger__c disableTrigger = (DisableTrigger__c) TestFactory.createSObject(new DisableTrigger__c(),
                                                         'TestFactory.DisableTriggerDefaults', true);
    	//Contact con = (Contact) TestFactory.createSObject(new Contact(RecordTypeId = RecordTypeId),
                                                       // 'TestFactory.ContactDefaults', true);
    	Application__c application = (Application__c) TestFactory.createSObject(new Application__c(),
                                                         'TestFactory.ApplicationDefaults', true);
    	
    }

    public static void createEmailTemplate(){
    	//EmailTemplate et = (EmailTemplate)TestFactory.createSObject(new EmailTemplate(), 'TestFactory.EmailTemplateDefaults', true);
    	createContactObject();
    }

    public static void createContactObject(){
    	Id RecordTypeId = ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get('Applicant');
    	Contact con = (Contact)TestFactory.createSObject(new Contact(RecordTypeId = RecordTypeId),
                                                         'TestFactory.ContactDefaults', true);
    }
    
    public static void createTestDataForAttendenceController(){
        DisableTrigger__c disableTrigger = (DisableTrigger__c)TestFactory.createSObject(new DisableTrigger__c(),
                                                         'TestFactory.DisableTriggerDefaults', true);
        Id RecordTypeId = ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get('Applicant');
    	Contact con = (Contact)TestFactory.createSObject(new Contact(RecordTypeId = RecordTypeId),
                                                         'TestFactory.ContactDefaults', true);
        /*Events__c event = (Events__c)TestFactory.createSObject(new Events__c(),
                                                         'TestFactory.EventsDefaults', true);*/
        Class__c cls = (Class__c)TestFactory.createSObject(new Class__c(),
                                                         'TestFactory.ClassDefaults', true);
        Class_Roster__c clsroster = (Class_Roster__c)TestFactory.createSObject(new Class_Roster__c(Class__c = cls.id,Contact__c = con.id),
                                                         'TestFactory.Class_RosterDefaults', true);
                                                      
        /*Attendance__c attendence = (Attendance__c)TestFactory.createSObject(new Attendance__c(Attendee__c = con.Id,
                                                                                              Class_Roster__c = clsroster.id),
                                                         'TestFactory.AttendanceDefaults', true);*/
        
    }
    
    public static void createBasaDataForEventPP(){
        Id RecordTypeId = ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get('Applicant');
        Id leadID = ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get('Lead');
    	Contact con = (Contact)TestFactory.createSObject(new Contact(RecordTypeId = RecordTypeId),
                                                         'TestFactory.ContactDefaults', true);
        Events__c event = (Events__c)TestFactory.createSObject(new Events__c(),
                                                         'TestFactory.EventsDefaults', true);
        
    }

    public static void createTestDataForProgramEnrollment() {
    	DisableTrigger__c disableTrigger = (DisableTrigger__c)TestFactory.createSObject(new DisableTrigger__c(),
                                                         'TestFactory.DisableTriggerDefaults', true);
    	Id ConRecordTypeId 	= ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get('Applicant');
        Id PRecordTypeId 	= ObjectUtils.getRecordTypeIdsByDeveloperName(Program__c.sObjectType).get('HealthWorks');
        Id PERecordTypeId 	= ObjectUtils.getRecordTypeIdsByDeveloperName(Program_Enrollment__c.sObjectType).get('HealthWorks');

    	Contact con 						= (Contact) TestFactory.createSObject(new Contact(RecordTypeId = ConRecordTypeId),
                                                         'TestFactory.ContactDefaults', true);
        Application__c application 			= (Application__c) TestFactory.createSObject(new Application__c(ApplicantId__c = con.Id, Stage__c = 'Interview'),
                                                         'TestFactory.ApplicationDefaults', true);
        Program__c program 					= (Program__c) TestFactory.createSObject(new Program__c(RecordTypeId = PRecordTypeId),
                                                         'TestFactory.ProgramDefaults', true);
        Program_Enrollment__c progEnroll 	= (Program_Enrollment__c) TestFactory.createSObject(new Program_Enrollment__c(RecordTypeId = PERecordTypeId, Program__c = program.Id,Client__c = con.Id),
                                                         'TestFactory.ProgramEnrollmentDefaults', true);
    }
    
    public static void createBaseDataForInterview(){
        DisableTrigger__c disableTrigger = (DisableTrigger__c)TestFactory.createSObject(new DisableTrigger__c(),
                                                         'TestFactory.DisableTriggerDefaults', true);
    	Id ConRecordTypeId 	= ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get('Applicant');
        Id PRecordTypeId 	= ObjectUtils.getRecordTypeIdsByDeveloperName(Program__c.sObjectType).get('HealthWorks');
        Id PERecordTypeId 	= ObjectUtils.getRecordTypeIdsByDeveloperName(Program_Enrollment__c.sObjectType).get('HealthWorks');

    	Contact con 						= (Contact) TestFactory.createSObject(new Contact(RecordTypeId = ConRecordTypeId),
                                                         'TestFactory.ContactDefaults', true);
        Application__c application 			= (Application__c) TestFactory.createSObject(new Application__c(ApplicantId__c = con.Id, Stage__c = 'Interview'),
                                                         'TestFactory.ApplicationDefaults', true);
        Interview__c  interview	= (Interview__c) TestFactory.createSObject(new Interview__c(Application__c = application.Id, ApplicantId__c = con.id),
                                                         'TestFactory.InterviewDefaults', true);   
    }
	public static void createBaseTestForClassRosterTgiggerHandler(){
		Id RecordTypeId = ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get('Applicant');
    	DisableTrigger__c disableTrigger = (DisableTrigger__c) TestFactory.createSObject(new DisableTrigger__c(),
                                                         'TestFactory.DisableTriggerDefaults', true);
    	Contact con = (Contact) TestFactory.createSObject(new Contact(RecordTypeId = RecordTypeId),
                                                         'TestFactory.ContactDefaults', true);
		Class__c cls = (Class__c) TestFactory.createSObject(new Class__c(),
                                                         'TestFactory.ClassDefaults', true);

		Class_Roster__c classRoster = (Class_Roster__c) TestFactory.createSObject(new Class_Roster__c(Class__c = cls.id, Contact__c = con.id ),
                                                         'TestFactory.Class_RosterDefaults', true);

	}
    
    public static void createBaseDataForAttendanceTesting(){
        DisableTrigger__c disableTrigger = (DisableTrigger__c) TestFactory.createSObject(new DisableTrigger__c(),
                                                         'TestFactory.DisableTriggerDefaults', true);
    	Id ConRecordTypeId 	= ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get('Applicant');
    	Contact con = (Contact) TestFactory.createSObject(new Contact(RecordTypeId = ConRecordTypeId), 'TestFactory.ContactDefaults', true);
        Program__c program = (Program__c) TestFactory.createSObject(new Program__c(), 'TestFactory.ProgramDefaults', true);
        Class__c cls = (Class__c)TestFactory.createSObject(new Class__c(Program__c = 'HealthWorks'),'TestFactory.ClassDefaults', true);
        Class_Roster__c clsroster = (Class_Roster__c)TestFactory.createSObject(new Class_Roster__c(Class__c = cls.id), 'TestFactory.Class_RosterDefaults', true);

    }
}