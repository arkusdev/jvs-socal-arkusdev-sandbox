/*
	Use : This is scheduler class to schedule BatchToSendMissyouMail
	Description: This batch class is used for Scheduling the batch BatchToSendInterviewReminders
					 
*/
global class BatchToSendMissyouMailScheduler implements Schedulable {
    global void execute(System.SchedulableContext sc){ 
        BatchToSendMissyouMail Obj = new BatchToSendMissyouMail();
        Database.executeBatch(Obj);
    }
}