//**

// Author: Mazuryk Yaroslav
// Date: December 28, 2018
// Description: << this code for testing EmailHelper class>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org


@isTest
public class EmailHelperTest {

	@testSetup static void setup(){
		TestFactory.createEmailTemplate();
	}

	@isTest
	public static void sendEmail_Test_01(){

		List<EmailTemplate> etList = [SELECT id,Name FROM EmailTemplate];
		List<Contact> conList = [SELECT id, Name, Email FROM Contact];
		Test.startTest();
		EmailHelper.sendEmail(etList.get(0).Name,conList.get(0).id);
		Test.stopTest();
	}

    @isTest
	public static void sendEmail_Test_02(){
		List<String> toAdress = new List<String>();
		toAdress.add('test@gmail.com');
		toAdress.add('test2@gmail.com');

		List<EmailTemplate> etList = [SELECT id,Name FROM EmailTemplate];
		List<Contact> conList = [SELECT id, Name, Email FROM Contact];
		Test.startTest();
		EmailHelper.sendEmail(etList.get(0).Name,conList.get(0).id,toAdress);
		Test.stopTest();
	}

}