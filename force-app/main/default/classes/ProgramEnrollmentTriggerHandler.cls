//**

// Author: Mazuryk Yaroslav
// Date: December 13, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
// 
public class ProgramEnrollmentTriggerHandler {

	public static void entry(TriggerParams triggerParams) {
        Map <Id, Program_Enrollment__c> newMap 	= (Map <Id, Program_Enrollment__c>) triggerParams.newMap;
        Map <Id, Program_Enrollment__c> oldMap 	= (Map <Id, Program_Enrollment__c>) triggerParams.oldMap;
        List <Program_Enrollment__c> triggerNew = (List <Program_Enrollment__c>) triggerParams.triggerNew;
        List <Program_Enrollment__c> triggerOld = (List <Program_Enrollment__c>) triggerParams.triggerOld;
        if (triggerParams.isBefore) {
        	if (triggerParams.isDelete) {
                updateClientStageToInterview(triggerOld);
            }
        } 
        if (triggerParams.isAfter) {
            if (triggerParams.isInsert) {
                updateClientStageToPE(triggerNew);
            }  
        }
    }

    public static void updateClientStageToPE(List<Program_Enrollment__c> triggerNew) {
    	Set<Id> clientIds = new Set<Id>();
    	for (Program_Enrollment__c enrollment : triggerNew) {
    		clientIds.add(enrollment.Client__c);
    	}
        
        List<Application__c> appList =  [select Id, Stage__c from Application__c where ApplicantId__c in :clientIds];
        Set<Id> applicationIds = new Set<Id>();
        for(Application__c a : appList){
			applicationIds.add(a.id);            
        }
         List<Interview__c> intList =  [select Id, Stage_1__c,Application__c from Interview__c where Application__c in :applicationIds];
        Set<Id> appToUpdate = new Set<Id>();
        
        for(Interview__c i : intList){
            if(i.Stage_1__c == Constants.STAGE_ORIENTATION_INTERVIEW){                
                appToUpdate.add(i.Application__c);
            }
        }       
    	List<Application__c> toUpdate = new List<Application__c>();
    	for (Application__c app : [select Id, Stage__c from Application__c where id in :appToUpdate]) {           
                app.Stage__c = Constants.STAGE_PROGRAM_ENROLLMENT;
                toUpdate.add(app);
    	}
    	update toUpdate;
    }

    public static void updateClientStageToInterview(List<Program_Enrollment__c> triggerOld) {
    	Set<Id> clientIds = new Set<Id>();
    	for (Program_Enrollment__c enrollment : triggerOld) {
            if(enrollment.Client__c != null){
    		clientIds.add(enrollment.Client__c);
            }   
    	}

        if(!clientIds.isEmpty()){
        List<Application__c> toUpdate = new List<Application__c>();
    	for (Application__c app : [select Id, Stage__c from Application__c where ApplicantId__c in :clientIds]) {
    		app.Stage__c = Constants.STAGE_REVIEW;
    		toUpdate.add(app);
    	}
            if(!toUpdate.isEmpty()){
              update toUpdate;
            }

        }

    }

}