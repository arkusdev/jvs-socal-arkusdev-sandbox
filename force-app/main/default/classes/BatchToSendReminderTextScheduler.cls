/*
    Use : This is scheduler class to schedule BatchToSendInterviewReminderText 
    Description: This batch class is used for Scheduling the batch BatchToSendInterviewReminderText
                     
*/
global class BatchToSendReminderTextScheduler implements Schedulable{
    global void execute(System.SchedulableContext sc){ 
        BatchToSendInterviewReminderText Obj = new BatchToSendInterviewReminderText();
        Database.executeBatch(Obj,1);
    }
}