//**

// Author: Mazuryk Yaroslav

// Date: December 27, 2018

// Description: << this code allow you sent Email template from everywhere where do you wont>>

// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>

// This code is the property of Provisio Partners and copy or reuse is prohibited.

// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.

// support@provisiopartners.org

public class EmailHelper {

	public static void sendEmail(String EmailTemplate, id ObjectId,id ConId, List<String> setToAddresses ){

		String searchName = '%' + EmailTemplate  + '%';
		List<EmailTemplate>  etList = [SELECT Id, Name FROM EmailTemplate  WHERE Name LIKE :searchName];

  		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   			mail.setTemplateId(etList.get(0).Id);
    		mail.setToAddresses(setToAddresses);
    		mail.setTargetObjectId(ConId);
        	mail.setWhatId(ObjectId);
    		mail.setSaveAsActivity(false);
    		mail.setUseSignature(false);
        
		List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
			allmsg.add(mail);
		try {
		    Messaging.sendEmail(allmsg,false);
		    return;
		} catch (Exception e) {
		    System.debug(e.getMessage());
		}
	}

	public static void sendEmail(String EmailTemplate, id ObjectId){

		String searchName = '%' + EmailTemplate  + '%';
		List<EmailTemplate>  etList = [SELECT Id, Name FROM EmailTemplate  WHERE Name LIKE :searchName];


  		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   			mail.setTemplateId(etList.get(0).Id);
    		mail.setTargetObjectId(ObjectId);
    		mail.setSaveAsActivity(false);
    		mail.setUseSignature(false);
		List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
			allmsg.add(mail);
		try {
		    Messaging.sendEmail(allmsg,false);
		    return;
		} catch (Exception e) {
		    System.debug(e.getMessage());
		}

	}
    
    	public static void sendEmail(String EmailTemplate, id ObjectId, List<String> setToAddresses ){

		String searchName = '%' + EmailTemplate  + '%';
		List<EmailTemplate>  etList = [SELECT Id, Name FROM EmailTemplate  WHERE Name LIKE :searchName];


  		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   			mail.setTemplateId(etList.get(0).Id);
    		mail.setTargetObjectId(ObjectId);
            mail.setToAddresses(setToAddresses);
    		mail.setSaveAsActivity(false);
    		mail.setUseSignature(false);
		List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
			allmsg.add(mail);
		try {
		    Messaging.sendEmail(allmsg,false);
		    return;
		} catch (Exception e) {
		    System.debug(e.getMessage());
		}

	}


}