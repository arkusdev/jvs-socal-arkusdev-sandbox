//**
// Author: Shashank Parashar
// Date: July 26, 2018
// Description: LEX component Controller
// Revised:
//      Author: Mazuryk Yaroslav
//      Date: January 3, 2019
//      Description: Class was changed due to a difference with existing data model.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
//
public with sharing class DailyAttendanceController {
     //Method for fetching the records based on Date and RecordId(ClassId)
  
    @AuraEnabled
    public static List<Attendance__c> serverGetAttendanceRecords(Id classId, String dateString) {

       List<Attendance__c> attList =  [SELECT Id, Status__c, Date__c, Class_Roster__r.Class__c, Class_Roster__r.Contact__r.Name FROM Attendance__c 
        Where Date__c = :Date.valueOf(dateString) And Class_Roster__r.Class__c = :classId];

        return attList;
        
    }
    
        //Method for Updating the records
    @AuraEnabled
    public static void serverUpdateAttendanceRecord(List<Attendance__c> AttendanceUpdate) {
        update AttendanceUpdate;
    }

}