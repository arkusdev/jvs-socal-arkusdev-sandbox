//**
// Author: Yaroslav Mazuryk
// Date: March 26, 2018
// Description: LEX component to get picklist Controller
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
//
public class GetPicklistController {
    
    //Get picklist for Field with Parameter Object name and Field name
    @AuraEnabled
    public static List<String> getPicklistValues(String objectName, String fieldName) {
        
        // Describe the SObject using its object type, Get a map of fields for the SObject.
        Map<String, Schema.SObjectField> fieldMap =
            Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        
        // Get the list of picklist entries for this field.
        List<Schema.PicklistEntry> picklistEntries =
            fieldMap.get(fieldName).getDescribe().getPicklistValues();
        
        // Add the entry values to the list.
        List<String> picklistValues = new List<String>();
        for (Schema.PicklistEntry a: picklistEntries) {
            picklistValues.add(a.getValue());
        }
        picklistValues.sort();
        return picklistValues;
    }

}