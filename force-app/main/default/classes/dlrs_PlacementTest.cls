/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_PlacementTest
{
    @IsTest
    private static void testTrigger()
    {
Program_Enrollment__c pe = new Program_Enrollment__c();        
dlrs.RollupService.testHandler(new Placement__c(Program_Enrollment__c=pe.Id));
    }
}