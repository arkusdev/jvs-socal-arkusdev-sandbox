/*
	Use : This batch class is scheduled daily
	Description: This batch class is used for sending the InterviewReminder Text to the Applicant 1 day before his Interview.
					 
*/
global class BatchToSendInterviewReminderText implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext BC){
   		return Database.getQueryLocator([SELECT Id,Name,Email__c,Disposition__c,ApplicantId__c,ApplicantId__r.Name,ApplicantId__r.Email,FirstName__c,Cell_Phone__c,Other_Phone__c FROM Application__c WHERE Disposition__c LIKE: '%Invited%' ORDER BY LastModifiedDate DESC]);
   	}

   	global void execute(Database.BatchableContext BC, List<Application__c> scope){
   		if(scope.size() > 0){
   			Integer counter = 0;
   			DateTime mailSendDate = system.now();
   			//DateTime mailSendDate = DateTime.newInstance(2020, 1, 17);
   			/*if(Test.isRunningTest()){
   				//mailSendDate = DateTime.newInstance(2020, 12, 3);
   				mailSendDate = system.now().AddDays(-2);
   			}*/
   			while (true) {
   				mailSendDate = mailSendDate.addDays(1);
   				Boolean holidayDateCheck = UtilClassForInterviewMails.calBusinessDays(mailSendDate,'InterviewReminderDates');
   				if(holidayDateCheck){
   					counter = counter + 1;
   				}
   				if(counter == 1){
   					break;
   				}
   			}
   			if(counter == 1){
   				List<Application__c> filteredInterviewList = new List<Application__c>();
   				for(Application__c s : scope){
   					String str = s.Disposition__c;
   					if(str.split(' ').size() > 3){
   						String loc = str.split(' ')[1];
						String tempDt = '';
						if(str.split(' ').size() > 2){
							tempDt = str.split(' ')[2];
						}
						String tempTi = '';
						if(str.split(' ').size() > 3){
							tempTi = str.split(' ')[3];
						}
						String timestr = tempTi;
						if(str.split(' ').size() > 4){
						    String amPm = str.split(' ')[4];
						    timestr = timestr + ' ' + amPm;
						    timestr = timestr.replaceAll('-', '');
						}
						String endTime = '';
						if(str.contains('-')){
							endTime = str.substringAfter('-');
						}
						if(tempDt != null && tempDt != ''){
							String month1 = tempDt.split('/')[0];
							String day1 = tempDt.split('/')[1];
							String year1 = '';
                            if(tempDt.split('/').size() > 2){
                            	year1 = tempDt.split('/')[2];
                            }
                            if(year1 != null && year1 != '' && month1 != null && month1 != '' && day1 != null && day1 != ''){
								DateTime dispositionDate = DateTime.newInstance(integer.valueof(year1), integer.valueof(month1), integer.valueof(day1));
								Boolean holidayDateCheck = UtilClassForInterviewMails.calBusinessDays(system.now(),'InterviewReminderDates');
								if(Test.isRunningTest()){
	                            	mailSendDate = dispositionDate;
	                            }
								if(dispositionDate.date() == mailSendDate.date() && holidayDateCheck != null && holidayDateCheck){
				     		 		filteredInterviewList.add(s);	
				     		 	}
                            }
						}
					}
		     	}
		     	if(filteredInterviewList.size() > 0){
		     		List<tdc_tsw__Message_Template__c> templateList = new List<tdc_tsw__Message_Template__c>();
		     		templateList = [Select Id,Name,tdc_tsw__SMSBodyNew__c from tdc_tsw__Message_Template__c where Name = 'Interview Reminder' LIMIT 1];
		     		if(templateList.size() > 0){
		     			for(Application__c applicationObj : filteredInterviewList){
		     				if(applicationObj.Id != null && applicationObj.ApplicantId__c != null){
		     					String str = applicationObj.Disposition__c;
								String loc = str.split(' ')[1];
								String tempDt = str.split(' ')[2];
								String tempTi = str.split(' ')[3];
								String timestr = tempTi;
								if(str.split(' ').size() > 4){
								    String amPm = str.split(' ')[4];
								    timestr = timestr + ' ' + amPm;
								    timestr = timestr.replaceAll('-', '');
								}
								String endTime = '';
								if(str.contains('-')){
									endTime = str.substringAfter('-');
								}
								String month1 = tempDt.split('/')[0];
								String day1 = tempDt.split('/')[1];
								String year1 = '';
	                            if(tempDt.split('/').size() > 2){
	                            	year1 = tempDt.split('/')[2];
	                            }
	                            if(year1 != null && year1 != '' && month1 != null && month1 != '' && day1 != null && day1 != ''){
									DateTime dispositionDate = DateTime.newInstance(integer.valueof(year1), integer.valueof(month1), integer.valueof(day1));
									String templateHtmlValue = templateList[0].tdc_tsw__SMSBodyNew__c;
						     		date tempDate = dispositionDate.date();
									Integer month = tempDate.month();
									Integer day = tempDate.day();
									Integer year = tempDate.year();
									String dayOfWeek = Datetime.newInstance(year, month, day).format('EEEE');
									String monthText = Datetime.newInstance(year, month, day).format('MMMMM');
									String daySufix = '';
									    if(day == 01 || day == 21 || day == 31){daySufix = 'st';}
														else if(day == 02 || day == 22){daySufix = 'nd';}
															else if(day == 3 || day == 23){daySufix = 'rd';}
																else if(day == 4 || day >= 5 || day <= 20 || day >= 24 || day <= 30){daySufix = 'th';}
									String DateTextFormat= dayOfWeek+', '+ monthText + ' ' + day + daySufix +', ' + String.valueOf(year);
									String addressStr = '';
									if(loc != null && loc != ''){
										if(InterviewAddresses__c.getValues(loc) != null){
											addressStr = InterviewAddresses__c.getValues(loc).Full_Address__c;
	                            		}
									}
									templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWTEXTDATE', DateTextFormat);
						     		templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWDATE', dispositionDate.date().format());
									templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWTEXTSTARTTIME', timestr);
									templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWTEXTENDTIME', endTime);
									templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWADDRESS', addressStr);
									templateHtmlValue=templateHtmlValue.replaceAll('FIRSTNAME', applicationObj.FirstName__c);
									templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWDAY', dayOfWeek);
									templateHtmlValue=templateHtmlValue.replaceAll(']]>', '');
									templateList[0].tdc_tsw__SMSBodyNew__c = templateHtmlValue;
									if(applicationObj.Cell_Phone__c != null && applicationObj.ApplicantId__c != null){
										tdc_tsw.GlobalSMSSender.isSyncCall = true; 
				     					//tdc_tsw.GlobalSMSSender.SendSmsWithPhoneNumberAndMessageText(applicationObj.Cell_Phone__c,templateList[0].Id, applicationObj.Id);
				     					tdc_tsw.GlobalSMSSender.SendSmsWithPhoneNumberAndMessageText(applicationObj.Cell_Phone__c,templateHtmlValue, applicationObj.ApplicantId__c);
				     				}else if(applicationObj.Other_Phone__c != null && applicationObj.ApplicantId__c != null){
				     					tdc_tsw.GlobalSMSSender.isSyncCall = true;
				     					//tdc_tsw.GlobalSMSSender.SendSmsWithPhoneNumberAndMessageText(applicationObj.Other_Phone__c,templateList[0].Id, applicationObj.Id);
				     					tdc_tsw.GlobalSMSSender.SendSmsWithPhoneNumberAndMessageText(applicationObj.Other_Phone__c,templateHtmlValue, applicationObj.ApplicantId__c);
				     				}
	                            }
							}
		     			}
			     	}
		     	}
   			}
   		}
    }

   	global void finish(Database.BatchableContext BC){
   	}
}