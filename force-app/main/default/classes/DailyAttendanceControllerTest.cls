@IsTest
public class DailyAttendanceControllerTest {

    @testsetup
    public static void setup(){
        TestFactory.createTestDataForAttendenceController();       
    }
    
    @isTest
    public static void DailyAttendanceController_Test(){
        String datestring = String.valueOf(Date.today());
        Class__c cls = [SELECT id,Name FROM Class__c];
        test.startTest();
        List<Attendance__c> attList = DailyAttendanceController.serverGetAttendanceRecords(cls.Id,datestring);
        test.stopTest();
        
        system.assertEquals(1, attList.size());
        system.assertEquals('Enrolled', attList.get(0).status__c);
    }
    
        @isTest
    public static void serverUpdateAttendanceRecord_Test(){
        String datestring = String.valueOf(Date.today());
        List<Attendance__c> attList = [SELECT id, Name FROM Attendance__c];
        
        test.startTest();
        DailyAttendanceController.serverUpdateAttendanceRecord(attList);
        test.stopTest();
        
    }
}