//**
// Author: Mazuryk Yaroslav
// Date: January 11, 2019
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org


public class ClassRosterTriggerHandler {
        public static void entry(TriggerParams triggerParams) {
        List<Class_Roster__c> triggerNew = (List<Class_Roster__c>)triggerParams.triggerNew;
        List<Class_Roster__c> triggerOld = (List<Class_Roster__c>)triggerParams.triggerOld;
        Map<Id, Class_Roster__c> oldMap = (Map<Id, Class_Roster__c>)triggerParams.oldMap;
        Map<Id, Class_Roster__c> newMap = (Map<Id, Class_Roster__c>)triggerParams.newMap;

        if(triggerParams.isAfter && triggerParams.isUpdate) {
            updateAttendanceRecords(newMap, oldMap);
        }
        if(triggerParams.isAfter && triggerParams.isInsert) {
            createNewAttendanceRecords(triggerNew, false, oldMap);
        }
        if(triggerParams.isBefore && triggerParams.isInsert) {
            checkStartDayOnClass(triggerNew);
        }
        if(triggerParams.isBefore && triggerParams.isUpdate) {
            
        }
    }
        public static void createNewAttendanceRecords(List<Class_Roster__c> newClassRosters, Boolean isUpdt, Map<Id, Class_Roster__c> oldMapRoster) {

        Set<Id> classIds = new Set<Id>();
        for (Class_Roster__c cls : newClassRosters) {
            if (cls.Status__c == 'Training') {
                classIds.add(cls.Class__c);
            }
        }

        List<Class__c> classList = [Select Id, Name, Start_Date__c, End_Date__c, Monday__c, Tuesday__c, Wednesday__c, 
        Thursday__c, Friday__c, Saturday__c, Sunday__c From Class__c Where Status__c = 'In Progress' AND Id in :classIds AND Auto_Create_Attendance__c = True];
        
        Map<Id, List<Integer>> classMap = new Map<Id, List<Integer>>();
        if (!classList.isEmpty()) {
            for(Class__c cls : classList) {
                List<Integer> days = new List<Integer>();
                if (cls.Sunday__c) days.add(0);
                if (cls.Monday__c) days.add(1);
                if (cls.Tuesday__c) days.add(2);
                if (cls.Wednesday__c) days.add(3);
                if (cls.Thursday__c) days.add(4);
                if (cls.Friday__c) days.add(5);
                if (cls.Saturday__c) days.add(6);
                classMap.put(cls.Id, days);

            }
        }
        
        List<Attendance__c> attToInsrt = new List<Attendance__c>();
        Date startOfWeek = Date.today().toStartOfWeek();
        Date today = Date.today();
       
        if (!classList.isEmpty()) {
            for (Class_Roster__c cr : newClassRosters) {    
                if (!classMap.get(cr.Class__c).isEmpty() && cr.Status__c == 'Training') {
                        for (Integer day : classMap.get(cr.Class__c)) {
                        Date dateOfClass = startOfWeek.addDays(day);                   

                        if (isUpdt) {                       
                            if (dateOfClass > oldMapRoster.get(cr.Id).End_Date__c && dateOfClass <= cr.End_Date__c) {
                                Attendance__c atnd = new Attendance__c(
                                Status__c = 'Enrolled',
                                Class_Roster__c = cr.Id,
                                Date__c = dateOfClass,
                                System_Created__c = true,
                                Attendee__c = cr.Contact__c);
                                attToInsrt.add(atnd);
                            }
                        } else {  
                                            
                            if (startOfWeek <= dateOfClass) {
                                Attendance__c atnd = new Attendance__c(
                                Status__c = 'Enrolled',
                                Class_Roster__c = cr.Id,
                                Date__c = dateOfClass,
                                System_Created__c = true,
                                Attendee__c = cr.Contact__c);
                                attToInsrt.add(atnd);
                                Integer a = 0;
                                a++;
                            }
                        }
                    }
                }
            }
        }
          
        if (!attToInsrt.isEmpty()) {
            insert attToInsrt;
        }
    }


    public static void updateAttendanceRecords(Map<Id, Class_Roster__c> newCRMap, Map<Id, Class_Roster__c> oldCRMap) {
        
        List<Class_Roster__c> crToUpdateNew = new List<Class_Roster__c>();
        Map<Id, Class_Roster__c> crToUpdateOld = new Map<Id, Class_Roster__c>();
        List<Class_Roster__c> crToDelete = new List<Class_Roster__c>();
        Set<Id> attId = new Set<Id>();

        for ( Id crId : newCRMap.keySet()) {
            if( newCRMap.get( crId ).End_Date__c != oldCRMap.get( crId ).End_Date__c ) {
                if ( newCRMap.get( crId ).End_Date__c < oldCRMap.get( crId ).End_Date__c ) {
                    attId.add(crId);
                    crToDelete.add(newCRMap.get(crId));
                }
                if ( newCRMap.get( crId ).End_Date__c > oldCRMap.get( crId ).End_Date__c ) {
                    crToUpdateNew.add(newCRMap.get(crId));
                    crToUpdateOld.put(crId, oldCRMap.get(crId));
                }
            }
        }

        //Delete unnecessary Attendance Records
        if (!crToDelete.isEmpty()) {
            List<Attendance__c> atToDelete = new List<Attendance__c>();
            List<Attendance__c> atPreDelete = new List<Attendance__c>();

            atPreDelete = [Select Id, Date__c from Attendance__c Where Class_Roster__c in :attId];
            for (Attendance__c atn : atPreDelete) {
                for (Class_Roster__c cr : crToDelete)
                    if (atn.Date__c > cr.End_Date__c) {
                        atToDelete.add(atn);
                    }
            }
            if (!atToDelete.isEmpty()) {
                delete atToDelete;
            }
        }
        //Create missing Attendance Records
        if (!crToUpdateNew.isEmpty()) {
            createNewAttendanceRecords(crToUpdateNew, true, crToUpdateOld);
        }   
    }
    
    public static void checkStartDayOnClass(List<Class_Roster__c> classRList){
        Set<Id> classId = new Set<Id>();       
        for(Class_Roster__c cr: classRList){
            classId.add(cr.Class__c);
        }
        List<Class__c> classList = [SELECT Id,Start_Date__c FROM Class__c WHERE iD IN: classId];
        Map<id,Date> mapToCompare  = new Map<id,Date>();

        for(Class__c c : classList){
                mapToCompare.put(c.id,c.Start_Date__c);
        }
        
        for(Class_Roster__c cr: classRList){
            if(cr.Start_Date__c < mapToCompare.get(cr.Class__c)){
                Date DateToPrint = mapToCompare.get(cr.Class__c);
                cr.AddError('Your class will start on' +'  '+ DateToPrint +'  Please enter later Start Date');                
            }
        }
        
        
    }

}