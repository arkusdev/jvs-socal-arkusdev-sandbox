public class EventPpWrapper {
	@AuraEnabled public Date dateOfEvent; 
    @AuraEnabled public String status ;
    @AuraEnabled public Id saferEventId ;
}