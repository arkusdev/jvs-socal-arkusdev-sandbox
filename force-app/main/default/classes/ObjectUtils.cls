//**

// Author: Mazuryk Yaroslav

// Date: December 31, 2018

// Description: << Brief description of the code>>

// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>

//

// This code is the property of Provisio Partners and copy or reuse is prohibited.

// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.

// support@provisiopartners.org

public class ObjectUtils {

    public static Map<String, Id> getRecordTypeIdsByDeveloperName(Schema.SObjectType token) {
        Map<String, Id> rtMap = new Map<String, Id>();

        // Get the Describe Result
        Schema.DescribeSObjectResult obj = token.getDescribe();
        Map<String, Schema.RecordTypeInfo> recordTypeMap = obj.getRecordTypeInfosByDeveloperName();
        for(Schema.RecordTypeInfo rtInfo : recordTypeMap.values()){
            rtMap.put(rtInfo.getDeveloperName(), rtInfo.getRecordTypeId());
        }
        return rtMap;
    }
    
 //   public static String getSObjectTypeName (Id recordId) {
 //       return recordId.getSObjectType().getDescribe().getName();
 //   }

	//public static Id getRecordTypeId(String nameOfSObject, String recordTypeName){
 //       return Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
 //   }

 //   public static String getAllFieldsFromObject(String nameOfSObject){

 //       Map<String, Schema.SObjectField> sObjectFields = Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().fields.getMap();
 //       List<String> fields = new List<String>();

 //       for (String field: sObjectFields.keySet()) {
 //           if (sObjectFields.get(field).getDescribe().isAccessible()) {
 //                   fields.add(field);
 //           }
 //       }
 //       String query = 'SELECT ' + String.join(fields, ', ') + ' FROM ' + nameOfSObject;
 //       return query;
 //   }

}