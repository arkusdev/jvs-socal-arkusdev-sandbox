@IsTest public class BatchToSendInterviewRemindersTest {
	@IsTest public static void sendInterviewRemindersTest(){
       	insert new DisableTrigger__c(InterviewTrigger__c = true);
        
        Application__c appobj = new Application__c();
        appobj.FirstName__c = 'test First Name';
        appobj.LastName__c = 'test Last Name';
        appobj.Cell_Phone__c = '1234578965';
        appobj.Email__c = 'test@test.com';
        appobj.answer1__c = 'Facebook';
        appobj.answer2__c = 'Los Angeles';
        appobj.ZIP_Code__c = '99999';
        appobj.Disposition__c = 'CW Invited 08/28 @ 2';
        appobj.New_Orientation_Date__c = 'CW Invited 08/28 @ 2';
        appobj.Program__c = 'CareerWork$ Medical';
        insert appobj;
        // ApplicationTriggerForEmailHandler checks if Disposition__c contains 'Invited'
        appobj.Disposition__c = 'CW Invited 03/25 am';
        appobj.No_Show_at_Orientation__c = true;
        appobj.New_Orientation_Date__c = 'CW Invited 03/25 am';
        update appobj;
        
        tdc_tsw__Message_Template__c smsTemplate = new tdc_tsw__Message_Template__c();
        smsTemplate.Name = 'Interview Reminder';
        smsTemplate.tdc_tsw__SMSBodyNew__c = 'test Sms Body';
        insert smsTemplate;
        
        BatchToSendInterviewRemindersScheduler twoDayReminder = new BatchToSendInterviewRemindersScheduler();
        String sch1 = '0 0 10 1/1 * ? *';
        String jobID1 = system.schedule('Interview_ReminderTest', sch1, twoDayReminder);
        
        BatchToSendInterviewReminders objIntRem1 = new BatchToSendInterviewReminders();
        database.executeBatch(objIntRem1);
        
        BatchToSendMissyouMailScheduler twoDayMissYou = new BatchToSendMissyouMailScheduler();
        String sch2 = '0 0 11 1/1 * ? *';
        String jobID2 = system.schedule('MissYou_ReminderTest', sch2, twoDayMissYou);
        
        BatchToSendMissyouMail objIntRem2 = new BatchToSendMissyouMail();
        database.executeBatch(objIntRem2);
        
        BatchToSendReminderTextScheduler oneDayText = new BatchToSendReminderTextScheduler();
        String sch3 = '0 0 12 1/1 * ? *';
        String jobID3 = system.schedule('Interview_SMS_ReminderTest', sch3, oneDayText);
        
        BatchToSendInterviewReminderText objIntRem3 = new BatchToSendInterviewReminderText();
        database.executeBatch(objIntRem3);
        
        UtilClassForInterviewMails.calBusinessDays(null,null);
        
    }
}