//**

// Author: Mazuryk Yaroslav
// Date: December 26, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

@isTest
public class InterviewTriggerHandlerTest {
    @testSetup static void setup(){
	 	testfactory.createBaseDataForInterview();
	}

	@isTest
	public static void afterUpdate_Test_1(){
		List<Interview__c> inerList = [Select id, Name,ApplicantId__c,RecordTypeId,Full_Time__c,
                                       Job_Ready__c,Assistance__c,Understanding__c,Education_GED__c,
                                       Experiense_6_months__c,Available__c,Application__c,Invite_to_OPhase__c,Stage__c FROM Interview__c];

		Application__c application = [SELECT Id FROM Application__c WHERE Id = :inerList[0].Application__c];
		application.Program__c = 'ApartmentWorks';
		update application;

		Test.startTest();
    		InterviewTriggerHandler.createNewInterviewRecord(inerList);
		Test.stopTest();

		List<Interview__c> inerListAfterUpdate = [Select id, Name,ApplicantId__c,Stage__c FROM Interview__c];	
		for(Interview__c item:inerList){
			item.Dispositionss__c='Left message';
		}
		update inerList;
		//System.assertEquals(1,inerListAfterUpdate.size());
		//System.assertEquals('Phone Interview', inerListAfterUpdate.get(0).Stage__c);

	}
/*
	@isTest
	//positive test
	public static void afterUpdate_Test_2(){
		List<Interview__c> inerList = [Select id, Name,ApplicantId__c,RecordTypeId,Full_Time__c,
                                       Job_Ready__c,Assistance__c,Understanding__c,Education_GED__c,
                                       Experiense_6_months__c,Available__c,Application__c,Invite_to_OPhase__c,Stage__c FROM Interview__c];
		inerList.get(0).Experiense_6_months__c = true;
		inerList.get(0).Education_GED__c = true;
		inerList.get(0).Full_Time__c = true;
		inerList.get(0).Assistance__c = true;
		inerList.get(0).Available__c = true;
		inerList.get(0).Job_Ready__c = true;
		inerList.get(0).Understanding__c = true;
		inerList.get(0).Add_to_BannList__c = false;
		inerList.get(0).Invite_to_OPhase__c = true;
		update inerList;


		Test.startTest();
    		InterviewTriggerHandler.createNewInterviewRecord(inerList);
		Test.stopTest();

		List<Interview__c> inerListAfterUpdate = [Select id, Name,ApplicantId__c,RecordTypeId,Full_Time__c,
                                       Job_Ready__c,Assistance__c,Understanding__c,Education_GED__c,
                                       Experiense_6_months__c,Available__c,Application__c,Invite_to_OPhase__c,Stage__c FROM Interview__c];	

		System.assertEquals(2,inerListAfterUpdate.size());
	    System.assertEquals('In-person Interview',inerListAfterUpdate.get(0).Stage__c);

	}

		@isTest
	//negative test
	public static void afterUpdate_Test_3(){
		List<Interview__c> inerList = [Select id, Name,ApplicantId__c,RecordTypeId,Full_Time__c,
                                       Job_Ready__c,Assistance__c,Understanding__c,Education_GED__c,
                                       Experiense_6_months__c,Available__c,Application__c,Invite_to_OPhase__c,Stage__c FROM Interview__c];
		inerList.get(0).Experiense_6_months__c = true;
		inerList.get(0).Education_GED__c = true;
		inerList.get(0).Full_Time__c = false;
		inerList.get(0).Assistance__c = true;
		inerList.get(0).Available__c = true;
		inerList.get(0).Job_Ready__c = true;
		inerList.get(0).Understanding__c = true;
		inerList.get(0).Add_to_BannList__c = true;
		inerList.get(0).Invite_to_OPhase__c = true;
		update inerList;


		Test.startTest();
    		InterviewTriggerHandler.createNewInterviewRecord(inerList);
		Test.stopTest();

		List<Interview__c> inerListAfterUpdate = [Select id, Name,ApplicantId__c,Stage__c FROM Interview__c];	

		System.assertEquals(2,inerListAfterUpdate.size());
		System.assertEquals('Phone Interview',inerListAfterUpdate.get(0).Stage__c);

} */
}