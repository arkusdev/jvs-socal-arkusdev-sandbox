@isTest
public class EventPP_CtrlTest {
    @testSetup static void testsetup() {
		List<ContactWrapper> contactWrapperList = new List<ContactWrapper>();
        TestFactory.createBasaDataForEventPP();
	}

	@isTest 
	public static void nameofclassTest() {

        Events__c evtlist = [Select Id from Events__c limit 1];

		Test.startTest();
			String eventName = EventPP_Ctrl.nameofclass(evtlist.Id);
			System.assertEquals('Event', eventName);
		Test.stopTest();
	}

	@isTest
	public static void getContactWrappersTest() {
		Test.startTest();
			EventPP_Ctrl.getContactWrappers('Test');
		Test.stopTest();
	}

	@isTest
    public static void newConRecInsertTest(){
        List<Contact> newcontact = new List<Contact>();
        Contact c = new Contact();
        c.LastName = 'Tests';
        c.FirstName = 'Tests';
        c.Phone = '23424234';
        c.Email = 'aszxc@gmail.com';
        c.Rank__c             ='E-1';
        c.Era_of_service__c   = 'Unknown';
        newcontact.add(c);
            
		Events__c evtlist = [Select Id from Events__c limit 1];
		List<Contact> cntToUpd = [Select Id, LastName,Email,FirstName,Phone,LeadSource from Contact];

		Test.startTest();
			EventPP_Ctrl.newConRecInsert(newcontact, evtlist.Id, cntToUpd);
		Test.stopTest();
	}

	@isTest
	public static void newConRecInsertExceptionTest() {

		List<Contact> cntToInsrt = new List<Contact>();

		Events__c evtlist = [Select Id from Events__c limit 1];

		Test.startTest();
		try {
			EventPP_Ctrl.newConRecInsert(cntToInsrt, evtlist.Id, cntToInsrt);
		} catch(Exception ex) {
			Boolean expectedExceptionThrown =  ex.getMessage().contains('Please selected at least one contact, or enter the last name for at least one new contact.') ? true : false;
			System.assertEquals(expectedExceptionThrown, true);
		}
		Test.stopTest();
	}

	/*@isTest
	public static void newRefRecInsertTest() {

		List<Contact> cntToInsrt = new List<Contact>();

		Contact firstCnt = new Contact();
		firstCnt.LastName = 'TestLastName';
		cntToInsrt.add(firstCnt);
		Contact secondCnt = new Contact();
		secondCnt.LastName = 'TestSecondLastName';
		cntToInsrt.add(secondCnt);

		Events__c evtlist = [Select Id from Events__c limit 1];

		List<Contact> cntToUpd = [Select Id from Contact];


		Test.startTest();
			EventPP_Ctrl.newRefRecInsert(cntToInsrt, evtlist.Id, cntToUpd);
		Test.stopTest();
	}*/

	@isTest
	public static void newEventPpWrapperTest() {
		Test.startTest();
			EventPP_Ctrl.newEventPpWrapper();
		Test.stopTest();
	}

	/*@isTest
	public static void newRefRecInsertExceptionTest() {

		List<Contact> cntToInsrt = new List<Contact>();

		Events__c evtlist = [Select Id from Events__c limit 1];

		Test.startTest();
		try {
			EventPP_Ctrl.newRefRecInsert(cntToInsrt, evtlist.Id, cntToInsrt);
		} catch(Exception ex) {
			Boolean expectedExceptionThrown =  ex.getMessage().contains('Please selected at least one contact, or enter the last name for at least one new contact.') ? true : false;
			System.assertEquals(expectedExceptionThrown, true);
		}
		Test.stopTest();
	}*/

    

}