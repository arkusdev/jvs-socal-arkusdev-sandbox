//**
// Author: Mazuryk Yaroslav
// Date:   1 10 2019
// Description: This class is invoked from EventParticipation Lightning Component to create events attandance records.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public class EventPP_Ctrl {
    
    @AuraEnabled
    public static List<ContactWrapper> getContactWrappers(String searchKeyWord) {
        //searchKeyWord = String.escapeSingleQuotes(searchKeyWord.trim()) + '%\'';
        system.debug('Value in the search key word ' + searchKeyWord);
        String namelike = '%'+searchKeyWord+'%';
        List<ContactWrapper> contactWrapperList = new List<ContactWrapper>();
        List <Contact> listCon = [Select Id, Name, Birthdate, Account.Name, Phone, Email From Contact where Name like : namelike];
        System.debug('value in the list con ' + listCon);
        for (Contact eachContact : listCon) {
            contactWrapperList.add(new ContactWrapper(false,
                    eachContact.Name, eachContact.Id));
        }
        return contactWrapperList;
    }
    
    @AuraEnabled
    Public static String nameofclass(Id evtId) {
        System.debug('Value in the event Id ' + evtId);
        List<Events__c> evtList = new List<Events__c>();
        evtlist = [Select id, Name from Events__c where id = :evtId];
        return evtlist[0].Name;
    }
	 
    @AuraEnabled
    public static void newConRecInsert(List<Contact> newContacts, Id eventId, List<Contact> selectedContacts) {
        Id rtId;
         system.debug('newConRecInsert');
		List<RecordType> rtIds = [SELECT Id from RecordType where developerName = 'Lead' and sObjectType = 'Contact'];
        if (rtIds == null || rtIds.size() == 0) {
               System.debug('ERROR: Record Type not found');
           } else {
               rtId = rtIds[0].Id;
           }
        system.debug(rtId);
           
        // Create new contacts from fields entered on component.
        List<Contact> contactsToInsert = new List<Contact>();
        List<Attendance__c> attendanceToInsert = new List<Attendance__c>();
        
        for (Contact eachContact : newContacts) {
            // remove blank contacts
            if (eachContact == null) continue;
            if (String.isBlank(eachContact.LastName)) continue;

            contactsToInsert.add(new Contact(
                    FirstName = eachContact.FirstName,
                    LastName = eachContact.LastName,
                    Email = eachContact.Email,
                    Phone = eachContact.Phone,
                    Rank__c             ='E-1',
                    Era_of_service__c   = 'Unknown',
                	RecordTypeId = rtId
                    
            ));
        }
        
         insert contactsToInsert;
        
        for(Contact c : contactsToInsert){
            attendanceToInsert.add(new Attendance__c(
                Attendee__c = c.id,
            	Event_Attended__c = eventId,
                System_Created__c = True
            ));
        } 

        if (selectedContacts.isEmpty() && contactsToInsert.isEmpty()) {
            throwAuraException('Please selected at least one contact, or enter the last name for at least one new contact.');
        }

       
        insert attendanceToInsert;

        // consolidate contact lists
        List<Contact> attendanceContacts = new List<Contact>();
        //attendanceContacts.addAll(selectedContacts);
        attendanceContacts.addAll(contactsToInsert);
        system.debug(selectedContacts);
        // iterate over consolidated list to create attendances
        List<Attendance__c> attendances = new List<Attendance__c>();
        for (Contact eachContact : selectedContacts) {
            // sanitation check, even though this theoretically shouldn't happen 
            if (eachContact == null) continue;
            if (eachContact.Id == null) continue;
            
            attendances.add(new Attendance__c(
                	Attendee__c = eachContact.Id,
                	Event_Attended__c = eventId,
                    System_Created__c = True
            ));
        }
        insert attendances;
        
        
        
        
        List<Attendance__c> listAttendanceToCalculateRecod = [SELECT id,Event_Attended__c FROM Attendance__c WHERE Event_Attended__c =: eventId];
        List<Events__c> eventsToUpdate = [SELECT id,Count_Attendes__c FROM Events__c WHERE id =: eventId];
        
        for(Events__c e :eventsToUpdate){
            e.Count_Attendes__c = listAttendanceToCalculateRecod.size();
        }
        system.debug(eventsToUpdate);
        update eventsToUpdate;
    }
     
    
    public static void throwAuraException(String message) {
        AuraHandledException e = new AuraHandledException(message);
        e.setMessage(message);
        System.debug(e);
        throw e;
    }

    @AuraEnabled
    public static EventPpWrapper newEventPpWrapper() {
        EventPpWrapper wrp = new EventPpWrapper();
        wrp.saferEventId = null;
        return wrp ;
    }

    @AuraEnabled
    public static void insertAttdence(String contactWrappersJSON, String attdenceWrapperJSON) {

        if (!string.isBlank(contactWrappersJSON) && !string.isBlank(attdenceWrapperJSON)) {

            List<contactWrapper> contactWrapperList =
                    (List<ContactWrapper>) System.JSON.deserialize(contactWrappersJSON, List<ContactWrapper>.class);

            EventPpWrapper EventPpWrap = (EventPpWrapper) System.JSON.deserialize(attdenceWrapperJSON, EventPpWrapper.class);

            //Perform Operation with records

            List<Event_PP__c> newEventPp = new List<Event_PP__c>();
            for (ContactWrapper contactWrap : contactWrapperList) {
                if (contactWrap.isSelected) {
                    newEventPp.add(new Event_PP__c(
                            Event__c = EventPpWrap.saferEventId,
                          	Participant__c = contactWrap.contactId,
                            System_Created__c = True
                    ));
                }
            }
            insert newEventPp;

        }
    }
    
    @InvocableMethod
    public static void calculateAttendanceRecord(List<id> ids){
        List<Attendance__c> listAttendanceToCalculateRecod = [SELECT id,Event_Attended__c FROM Attendance__c WHERE Event_Attended__c IN: ids];
        List<Events__c> eventsToUpdate = [SELECT id,Count_Attendes__c FROM Events__c WHERE id =: ids];
        
        for(Events__c e :eventsToUpdate){
            e.Count_Attendes__c = listAttendanceToCalculateRecod.size();
        }
        system.debug(eventsToUpdate);
        update eventsToUpdate;
        
    }
}