//**

// Author: Mazuryk Yaroslav
// Date: December 13, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
public class InterviewUtils {
  public static List<Interview__c> createInterviewRecord(List<Interview__c> interList){
    List<Interview__c> intervewListToReturn  = new List<Interview__c>();

    // set ids to prevent dublicate record
    Set<Id> applicationIDs = new Set<Id>();
    for(Interview__c inter : interList){
      applicationIDs.add(inter.Application__c);
    }

    Id InterviewRecordType = ObjectUtils.getRecordTypeIdsByDeveloperName(Interview__c.sObjectType).get(Constants.RECORDTYPE_ORIENTATION_INTERVIEW);

    // get List have to be empty
    List<Interview__c> iListToPreventDublicate = [SELECT Id, RecordTypeId,Application__c FROM Interview__c WHERE  (Application__c IN: applicationIDs) AND (RecordTypeId =:InterviewRecordType)];
   
    if(iListToPreventDublicate.isEmpty()) {
      for(Interview__c inter : interList){
        if( inter.Invite_to_OPhase__c == true && inter.Add_to_BannList__c == false && inter.Stage__c == Constants.STAGE_PHONE_INTERVIEW){
                Interview__c interviewsToInsert = new Interview__c(
                Stage_1__c 			 	        = Constants.STAGE_ORIENTATION_INTERVIEW,
                Applicantid__c 			        = inter.Applicantid__c,
                RecordTypeId 		      	    = InterviewRecordType,
                Full_Time__c 			        = inter.Full_Time__c,
                Job_Ready__c 			        = inter.Job_Ready__c,
                Assistance__c 			        = inter.Assistance__c,
                Understanding__c		        = inter.Understanding__c,
                Education_GED__c 		        = inter.Education_GED__c,
                Experiense_6_months__c          = inter.Experiense_6_months__c,		 		
                Available__c	                = inter.Available__c,
                Application__c 			        = inter.Application__c,
                Invite_to_OPhase__c             = false
              );
            insert interviewsToInsert;
            intervewListToReturn.add(interviewsToInsert);
        }
      }
    }

    //insert interviewsToInsert;
     //insert intervewListToReturn;
     return intervewListToReturn;

  }

  private static String substringName(String s){
	    String s1 = s.substringBeforeLast(' ');
	    return s1;
}  

}