@isTest
public class BatchAttendanceCreateHelperTest {

	@testSetup static void initialize() {
        TestFactory.createBaseDataForAttendanceTesting();
	}

	@isTest static void insertAttendanceTest() {

		List<Class__c> clsList = [Select Id, Monday__c,Program__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c, Sunday__c, End_Date__c from Class__c];
		
		Test.startTest();
			BatchAttendanceCreateHelper.insertAttendance(clsList);       
		Test.stopTest();
	}

}