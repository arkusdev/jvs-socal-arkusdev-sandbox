//**

// Author: Mazuryk Yaroslav
// Date: December 13, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved
// support@provisiopartners.org

public class InterviewTriggerHandler {

    public static void entry(TriggerParams triggerParams) {
        Map <Id, Interview__c> newMap  = (Map <Id, Interview__c>) triggerParams.newMap;
        Map <Id, Interview__c> oldMap  = (Map <Id, Interview__c>) triggerParams.oldMap;
        List <Interview__c> triggerNew = (List <Interview__c>) triggerParams.triggerNew;
        List <Interview__c> triggerOld = (List <Interview__c>) triggerParams.triggerOld;
        if (triggerParams.isBefore) {
            if (triggerParams.isInsert) { 
            
            }
 
        } 
        if (triggerParams.isAfter) {           
            if (triggerParams.isUpdate) {
              //  createNewInterviewRecord(triggerNew);
              updateDespositionField(triggerNew);
                
            }
            if(triggerParams.isInsert){
                sendEmail(triggerNew);  

            }
          
        }
    }   
    // create new record interview
	public static void createNewInterviewRecord(List<Interview__c> iList ) {
        List<Interview__c> listToUpdate = new List<Interview__c>();
        
            for(Interview__c i: [SELECT id,Invite_to_OPhase__c,Add_to_BannList__c,Stage__c,Application__c ,Name,ApplicantId__c,Full_Time__c, 
                                 Job_Ready__c,Assistance__c,Understanding__c,Education_GED__c,Experiense_6_months__c ,Available__c    FROM Interview__c WHERE id IN:iList]){
                if(i.Invite_to_OPhase__c == true && i.Add_to_BannList__c == false && i.Stage__c == Constants.STAGE_PHONE_INTERVIEW){
                i.Stage__c = Constants.STAGE_IN_PERSON_INTERVIEW; 
                listToUpdate.add(i);
            	}
            }
        update listToUpdate;
        List<Interview__c> i = InterviewUtils.createInterviewRecord(iList);
        }
                          


    
    //check after update Whom we need to send Email
public static void sendEmail(List<Interview__c> iList){

  Id InterviewRecordType = ObjectUtils.getRecordTypeIdsByDeveloperName(Interview__c.sObjectType).get('Orientation_Interview');
  List<Interview__c> sendToList = new List<Interview__c>();

  for(Interview__c interview : iList){
    if(interview.RecordTypeId == InterviewRecordType){
    sendToList.add(interview);
  }
}
  for(Interview__c interview : sendToList){
    if(interview.Invite_to_Assesment__c == true ){
      //EmailHelper.sendEmail('Yes_Email_after_Interview',interview.Applicantid__c, toAdress);
    }else{
      //EmailHelper.sendEmail('No_Email_after_Interview',interview.Applicantid__c, toAdress);
    }
  }
}
    
    public static void updateDespositionField(List<Interview__c> iList){
        system.debug(iList.get(0).Dispositionss__c);
        List<id> applicationIds = new List<id>();
        Map<id, String> mapToSaveDesposition = new Map<id, String>();
        
        for(Interview__c i: iList){
            if(i.Application__c != null){
            applicationIds.add(i.Application__c);
            mapToSaveDesposition.put(i.Application__c, i.Dispositionss__c);  
            }            
        }
        
        List<Application__c> appList = [SELECT id, Disposition__c FROM Application__c WHERE ID IN: applicationIds];
        
        for(Application__c a: appList){
            system.debug('mapValue->'+mapToSaveDesposition.get(a.Id));
            system.debug('Id->'+a.Id);
            a.Disposition__c = mapToSaveDesposition.get(a.Id);
        }
        system.debug(appList);
        update appList;
    }

}