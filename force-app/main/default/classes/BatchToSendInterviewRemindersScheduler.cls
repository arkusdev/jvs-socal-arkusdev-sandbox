/*
	Use : This is scheduler class to schedule BatchToSendInterviewReminders
	Description: This batch class is used for Scheduling the batch BatchToSendInterviewReminders
					 
*/
global class BatchToSendInterviewRemindersScheduler implements Schedulable {
    global void execute(System.SchedulableContext sc){ 
        BatchToSendInterviewReminders Obj = new BatchToSendInterviewReminders();
        Database.executeBatch(Obj);
    }
}