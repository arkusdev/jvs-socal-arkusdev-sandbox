//**

// Author: Mazuryk Yaroslav
// Date: December 12, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org


public class ApplicationUtils {
  //get contact where Name and Phone the same
  //debrecated method
/*   public static List<Contact> checkIfContactsIsCreated(String phone, String email, String FirstName, String LastName){
      List<Contact> contacts = [SELECT Id, Phone,FirstName,LastName, Email FROM Contact WHERE Phone LIKE: phone AND Email LIKE: email AND FirstName LIKE: FirstName AND LastName LIKE: LastName];
      return contacts;
  }*/
    
    
    public static Map<String,Id> checkifContactIsCreated_2(List<Application__c> applicantList){
       // Map to return
        Map<String,Id> toReturnMapIDContact = new Map<String,Id>();
        // set for limited search 
        Set<String> setEmailString = new Set<String>();
        //add Email to Set
        for(Application__c app: applicantList ){
            setEmailString.add(app.Email__c);            
        }        
        List<Contact> contacts = [SELECT Id, Phone,FirstName,LastName, Email FROM Contact WHERE Email IN : setEmailString]; 
        // check if condition is true
        for(Application__c app: applicantList){
            for(Contact c : contacts){
                if(app.Email__c          == c.Email 
                   && app.Cell_Phone__c  == c.Phone
                   && app.FirstName__c   == c.FirstName  
                   && app.LastName__c    == c.LastName){
                    toReturnMapIDContact.put(app.Email__c,c.Id);
                }
            }
        }
        return toReturnMapIDContact;   
                 
    }


 //Method for creating Contacts releted to applications   
    public static Set<Contact> createnNewContactForApplication(List<Application__c> applicantList){
        //get recordtype Id
        Id conRecId = ObjectUtils.getRecordTypeIdsByDeveloperName(Contact.sObjectType).get(Constants.RECORDTYPE_APPLICANT);

        //Map to return
        //Map<Id,List<Contact>> newMapToReturn = new Map<id, List<Contact>>();

        List<Contact> toInsertContactList = new List<Contact>();       
        for(Application__c app : applicantList){
           toInsertContactList.add(new Contact(
                    RecordTypeId               = conRecId,
                    FirstName                  = app.FirstName__c,
                    LastName                   = app.LastName__c,
                    Email                      = app.Email__c,
                    OtherPhone                 = app.Other_Phone__c,
                    Phone                      = app.Cell_Phone__c,
                    Additional_Email__c        = app.Secondary_Email__c,
                    MailingCity                = app.City__c,
                    MailingState               = app.State__c,
                    MailingStreet              = app.Street__c,
                    MailingPostalCode          = app.ZIP_Code__c,
               		Rank__c                    = 'E-1',
                    Era_of_service__c          = 'Unknown'
           
           ));            
        }		
        List<Database.SaveResult> srList = Database.insert(toInsertContactList, false);
        system.debug(srList);
        // for(Database.SaveResult dbr : srList){
        //     if(!dbr.isSuccess()){
        //         for(Database.Error duplicateError : dbr.getErrors()){
        //             Datacloud.DuplicateResult duplicateResult = ((Database.DuplicateError)duplicateError).getDuplicateResult();
        //         }
        //         Database.DMLOptions dml = new Database.DMLOptions();
        //         dml.DuplicateRuleHeader.AllowSave = true;
        //         List<Database.SaveResult> srList2 = Database.insert(toInsertContactList, dml);
        //     }
        // }
        for(Database.SaveResult svr : srList){
            if(!svr.isSuccess()){
                for(Database.Error error : svr.getErrors()){
                    System.debug('Error during record inserting->>>'+error.getMessage());
                }

            }
        }
        return new Set<Contact>(toInsertContactList);               
        
    }

    //create automatically interview record rerleted to this application
    public static void createInterviewRecordReletedToApplication(List<Application__c> appList){
        List<Id> applicationIds = new List<Id>();
        
        for(Application__c a : appList){
            applicationIds.add(a.id);
        }
        
        List<Interview__c> interivewList = [SELECT Id, Application__c FROM  Interview__c WHERE Application__C IN:  applicationIds];
                      
        List<Interview__c> toCreate = new List<Interview__c>();
        if(interivewList.isEmpty()){
        for(Application__c app: appList){
            if(app.Invite_to_Interview__c == true){
                    toCreate.add( new Interview__c(
                       Stage__c       = Constants.STAGE_PHONE_INTERVIEW,
                       Application__c = app.id,
                       Applicantid__c = app.Applicantid__c
                       ));           
            }
        }
        }
        if (!toCreate.isEmpty()) {
            insert toCreate;
        }
    }
}