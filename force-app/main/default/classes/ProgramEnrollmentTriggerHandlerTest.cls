//**

// Author: Mazuryk Yaroslav
// Date: December 13, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

@isTest
public class ProgramEnrollmentTriggerHandlerTest {

 	@testSetup static void setup(){
		TestFactory.createTestDataForProgramEnrollment();
	}

	@isTest
	public static void updateClientStageToPETest() {
		Id conId = [select Id from Contact limit 1].Id;
		List<Application__c> apps = [select Id, ApplicantId__c, Stage__c from Application__c];
		List<Program_Enrollment__c> progEnrolls = [select Id, Client__c from Program_Enrollment__c];

		System.assertEquals('Review', apps[0].Stage__c);

		progEnrolls[0].Client__c = conId;
		update progEnrolls;

		List<Application__c> appsUpdated = [select Id, ApplicantId__c, Stage__c from Application__c];
        System.assertEquals(1, appsUpdated.size());
		//System.assertEquals('Program Enrollment', appsUpdated[0].Stage__c);
	}

	@isTest
	public static void updateClientStageToInterviewTest() {
		Id conId = [select Id from Contact limit 1].Id;
		List<Application__c> apps = [select Id, ApplicantId__c, Stage__c from Application__c];
		List<Program_Enrollment__c> progEnrolls = [select Id, Program__c ,Client__c from Program_Enrollment__c];

		apps[0].Stage__c = 'Program Enrollment';
		update apps;

		progEnrolls[0].Client__c = conId;
		update progEnrolls;
		
		delete progEnrolls;

		List<Application__c> appsUpdated = [select Id, ApplicantId__c, Stage__c from Application__c];
        System.assertEquals(1, appsUpdated.size());
		//System.assertEquals('Interview', appsUpdated[0].Stage__c);

	}

}