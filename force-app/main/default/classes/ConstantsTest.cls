@isTest
public class ConstantsTest{

    @isTest
    public static void Constants_Test(){
        Test.startTest();
        Constants c = new Constants();
        c.test();
        
        //Name Email template
        System.assertEquals(Constants.AUTOMATICALLY_DISQUALIFICATION, 'Automatically Disqualification Email');
        System.assertEquals(Constants.HW_NO_LETTER, 'HW No Letter');
        System.assertEquals(Constants.YES_EMAIL_AFTER_INTERVEW, 'Yes_Email_after_Interview');
        System.assertEquals(Constants.NO_EMAIL_AFTER_INTERVEW, 'No_Email_after_Interview');
        
        // Stage of application 
        System.assertEquals(Constants.PHONE_INTERVIEW, '  Phone-Interview');
        System.assertEquals(Constants.STAGE_PHONE_INTERVIEW, 'Phone Interview');
        System.assertEquals(Constants.STAGE_INTERVIEW, 'Interview');
        System.assertEquals(Constants.STAGE_REVIEW, 'Review');
        System.assertEquals(Constants.STAGE_PROGRAM_ENROLLMENT, 'Program Enrollment');
        System.assertEquals(Constants.STAGE_ORIENTATION_INTERVIEW, 'Orientation- Interview');
        
        //general 
        System.assertEquals(Constants.GENERAL_NO, 'No');
        System.assertEquals(Constants.GENERAL_YES, 'Yes');
        
        //recordtypes
        System.assertEquals(Constants.RECORDTYPE_APPLICANT, 'JVS_Works_Client');
        System.assertEquals(Constants.RECORDTYPE_ORIENTATION_INTERVIEW, 'Orientation_Interview');
        
        //status
        System.assertEquals(Constants.CLASS_ROSTER_STATUS_TRAINNING, 'Training');
        System.assertEquals(Constants.CLASS_STATUS_IN_PROGRESS, 'In Progress');
        System.assertEquals(Constants.ATTENDANCE_STATUS_ENROLLED, 'Enrolled');
        System.assertEquals(Constants.STAGE_IN_PERSON_INTERVIEW, 'In-person Interview');
        
        Test.stopTest();
    }

}