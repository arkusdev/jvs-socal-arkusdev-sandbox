//**
// Author: Mazuryk Yaroslav
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public class GraduationComponentContrller {
    
     @AuraEnabled
    public static List<Contact> getClientRecords(Id classId, String dateString) {
        List<Class_Roster__c> crList = [select Id, Contact__r.Id, Contact__r.Name, Contact__r.Status__c from Class_Roster__c where Class__c =: classId];
      
        
         Set<Contact> contacts = new Set<Contact>();
          for(Class_Roster__c crl: crList){
            contacts.add(crl.Contact__r);
         }
        return new List<Contact>(contacts);
        
    }
    @AuraEnabled
     public static List<Class_Roster__c> getClassRosterRecords(Id classId, String dateString) {
        List<Class_Roster__c> crList = [SELECT Contact__r.Name, Status__c FROM Class_Roster__c WHERE CLass__c =: classId];
         system.debug(crList);
        return crList;
        
    }
    
    
        //Method for Updating the records
    @AuraEnabled
    public static void updateClientStatus(List<Contact> conList) {
        /* List<Class_Roster__c> crList = [select Id from Class_Roster__c where Class__c =: classId];
        
        for(Contact c: conList){
            if(c.Status__c == 'Alumni'){
                c.Class_Roster__c = crList.get(0).id;
            }
        }*/
        
        update conList;
    }

}