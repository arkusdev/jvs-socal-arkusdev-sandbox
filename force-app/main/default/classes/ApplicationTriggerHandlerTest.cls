//**
// Author: Mazuryk Yaroslav
// Date: December 26, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

@isTest
public class ApplicationTriggerHandlerTest {
    @testSetup static void setup(){
        testfactory.createApplicationBaseTestData();
    }
      @isTest
    public static void afterUpdate_Test_1(){
        
    	List<Application__c> apps = [select Id,   Email__c, Other_Phone__c,answer6__c,answer5__c,answer3__c, Cell_Phone__c,FirstName__c, Secondary_Email__c,
                                    City__c, State__c, Street__c, ZIP_Code__c, ApplicantId__c 
    								from Application__c];
       
       ApplicationTriggerHandler.checkApplication(apps);
           
    }

    @isTest
    public static void createContactForAppTest() {
    	
    	List<Application__c> apps = [select Id,   Email__c, Other_Phone__c,answer6__c,answer5__c,answer3__c, Cell_Phone__c,FirstName__c, Secondary_Email__c,
                                    City__c, State__c, Street__c, ZIP_Code__c, ApplicantId__c 
    								from Application__c];
        
        List<Contact> cList = [SELECT ID ,FirstName, Name From Contact];
       
        
        system.assertEquals(apps.get(0).ApplicantId__c, cList.get(0).id);
        system.assertEquals(apps.get(0).FirstName__c, cList.get(0).FirstName);
        
         
    

    }
      
    @isTest
    public static void afterUpdate_Test_2(){
        List<Application__c> appList = [Select id, Name,ApplicantId__c,answer3__c,answer4__c,answer5__c,
                                        answer6__c,answer7__c,
                                        answer8__c, Stage__c FROM Application__c];
   
        appList.get(0).Stage__c = Constants.STAGE_REVIEW ;
        appList.get(0).Invite_to_Interview__c = true;
        update appList;
                
        List<Application__c> appListAfterUpdate = [Select id, Name,ApplicantId__c, Stage__c FROM Application__c];
        //List<Interview__c> inerList = [Select id, Name FROM Interview__c];
                
        System.assertEquals(1, appListAfterUpdate.size());
        //System.assertEquals(1, inerList.size());
        //System.assertEquals('Interview', appListAfterUpdate.get(0).Stage__c);
        
    }
    



}