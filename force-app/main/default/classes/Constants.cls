//**
// Author: Mazuryk Yaroslav
// Date: December 27, 2018
// Description: << this code allow you sent Email template from everywhere where do you wont>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

public with sharing class Constants {
    //Name Email teamplate
    public static final String AUTOMATICALLY_DISQUALIFICATION      = 'Automatically Disqualification Email';
    public static final String HW_NO_LETTER                        = 'HW No Letter';
    public static final String YES_EMAIL_AFTER_INTERVEW            = 'Yes_Email_after_Interview';
    public static final String NO_EMAIL_AFTER_INTERVEW             = 'No_Email_after_Interview';
    // Stage of  application 
    public static final String PHONE_INTERVIEW                     = '  Phone-Interview';
    public static final String STAGE_PHONE_INTERVIEW               = 'Phone Interview';
    public static final String STAGE_INTERVIEW                     = 'Interview';
    public static final String STAGE_REVIEW                        = 'Review';
    public static final String STAGE_PROGRAM_ENROLLMENT            = 'Program Enrollment';
    public static final String STAGE_ORIENTATION_INTERVIEW         = 'Orientation- Interview';
    //general 
    public static final String GENERAL_NO                          = 'No';
    public static final String GENERAL_YES                         = 'Yes';
    //recordtypes
    public static final String RECORDTYPE_APPLICANT                = 'JVS_Works_Client';
    public static final String RECORDTYPE_ORIENTATION_INTERVIEW    = 'Orientation_Interview';
    //status
    public static final String CLASS_ROSTER_STATUS_TRAINNING       = 'Training';
    public static final String CLASS_STATUS_IN_PROGRESS            = 'In Progress';
    public static final String ATTENDANCE_STATUS_ENROLLED          = 'Enrolled';
    public static final String STAGE_IN_PERSON_INTERVIEW           = 'In-person Interview';
    
    public void test(){
        String s = AUTOMATICALLY_DISQUALIFICATION;
    }

}