//**
// Author: Mazuryk Yaroslav
// Date: January 11, 2019
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
@isTest
public class ClassRosterTriggerHandlerTest {
 @TestSetup
    public static void testsetup(){
       TestFactory.createBaseTestForClassRosterTgiggerHandler();
    }
   
   @isTest
    public static void checkIfAttendanceCreated(){
      List<Attendance__c> aList =  [SELECT id FROM Attendance__c];
     
     System.assertEquals(5,aList.size());
    } 
    
   @isTest
    public static void checkUpdateAttendanceRecord(){
        List<Class_Roster__c> clrList = [SELECT ID, Name, Status__c FROM Class_Roster__c];
        for(Class_Roster__c clr : clrList){
            clr.Status__c = 'Dropped Out';
            clr.End_Date__c = Date.today();
        }
        update clrList;
    }
}