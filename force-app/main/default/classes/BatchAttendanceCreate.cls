//**
// Author: Yaroslav Mazuryk
// Date: January 22, 2019
// Description: This is a Batch Class that creates Attendance records for a week for active classes.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

global class BatchAttendanceCreate implements Database.Batchable<sObject> {

	global Database.QueryLocator start(Database.BatchableContext BC) {

        String query = 'Select Id, Name, Start_Date__c, End_Date__c, Program__c, Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c, Sunday__c From Class__c Where Status__c = \'In Progress\' AND Auto_Create_Attendance__c = True';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Class__c> scope) {

    	BatchAttendanceCreateHelper.insertAttendance(scope);

    }

    global void finish(Database.BatchableContext BC) {}
}