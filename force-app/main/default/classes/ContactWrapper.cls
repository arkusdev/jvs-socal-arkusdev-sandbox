//**
// Author: Mazuryk Yaroslav
// Date: January 4, 2019
// Description: This wrapper is invoked from different apex classes in order to create ContactWrapper
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public with sharing class ContactWrapper {
    @AuraEnabled public Boolean isSelected;
    @AuraEnabled public String contactName;
    @AuraEnabled public String programName;
    @AuraEnabled public Id contactId;
    @AuraEnabled public List<Program_Enrollment__c>  programList;
    @AuraEnabled public Integer noOfAdult;
    @AuraEnabled public Id selectedProgm ;
    @AuraEnabled public Date birthdate;
    @AuraEnabled public String docNumb;

    public ContactWrapper(Boolean isSelected, String contactName, Id contactId, String docNumb) {
        this.isSelected = isSelected;
        this.contactName = contactName;
        this.contactId = contactId;
        this.docNumb = docNumb;
    } 
   
    public ContactWrapper(Boolean isSelected, String contactName, String programName, Id contactId,List<Program_Enrollment__c> programList, Id selectedProgm) {
        this(isSelected, contactName, programName, contactId,programList ,0, selectedProgm);
    } 
    
    public ContactWrapper(Boolean isSelected, String contactName, String programName, Id contactId, List<Program_Enrollment__c> programList, integer noOfAdult, Id selectedProgm){
        this(isSelected, contactName, programName, contactId, programList, noOfAdult, selectedProgm, null);
    }

    public ContactWrapper(Boolean isSelected, String contactName, String programName, Id contactId, List<Program_Enrollment__c> programList, integer noOfAdult, Id selectedProgm, Date birthdate){
        this.isSelected = isSelected;
        this.contactName = contactName;
        this.programName = programName;
        this.contactId = contactId;
        this.programList = programList;
        this.noOfAdult = noOfAdult;
        this.selectedProgm = selectedProgm;
        this.birthdate = birthdate;
        //this.docNumb = docNumb;
    }

     public ContactWrapper(Boolean isSelected, String contactName, Id contactId) {
        this.isSelected = isSelected;
        this.contactName = contactName;
        this.contactId = contactId;
    } 

    // When adding fields to this wrapper, do not update the parameters of any existing constructors. Add new fields
    // as needed, otherwise create new constructors to overload.
}