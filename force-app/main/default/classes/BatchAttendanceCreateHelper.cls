//**
// Author: Yaroslav Mazuryk
// Date: January 22, 2019
// Description: This is a Batch Class Helper that creates Attendance records for a week for active classes.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public class BatchAttendanceCreateHelper {

	public static void insertAttendance(List<Class__c> scope) {

		Date today = Date.today();

    	Map<Id, List<Integer>> classMap = new Map<Id, List<Integer>>();
    	Set<Id> classId = new Set<Id>();
        Map<Id, String> programMap = new Map<Id, String>();

        for(Class__c cls : scope) {
            List<Integer> days = new List<Integer>();
            if (cls.Sunday__c) days.add(0);
            if (cls.Monday__c) days.add(1);
            if (cls.Tuesday__c) days.add(2);
            if (cls.Wednesday__c) days.add(3);
            if (cls.Thursday__c) days.add(4);
            if (cls.Friday__c) days.add(5);
            if (cls.Saturday__c) days.add(6);
            classMap.put(cls.Id, days);
            classId.add(cls.Id);

            //Populate Map with Class Id and Program
            programMap.put(cls.Id, cls.Program__c);

        }
        List<Class_Roster__c> clsRoster = [Select Id, Class__c From Class_Roster__c Where Class__c in :classId And Start_Date__c <= :today 
            And Status__c = 'Training' And Class__r.Auto_Create_Attendance__c = True];
        List<Attendance__c> attToInsrt = new List<Attendance__c>();
        Date startOfWeek = Date.today().toStartOfWeek();

       // Id impactRecordTypeId = Schema.SObjectType.Attendance__c.getRecordTypeInfosByDeveloperName().get('Impact').getRecordTypeId();
      //  Id qcRecordTypeId = Schema.SObjectType.Attendance__c.getRecordTypeInfosByDeveloperName().get('QC').getRecordTypeId();
        Id standardRecordTypeId = Schema.SObjectType.Attendance__c.getRecordTypeInfosByDeveloperName().get('Standard').getRecordTypeId();

        if (!scope.isEmpty()) {
            for (Class_Roster__c cr : clsRoster) {

                String programName = programMap.get(cr.Class__c);
                Id recordType;
              //  if (programName == 'Impact') {
            //        recordType = impactRecordTypeId;
           //     } else if (programName == 'QC') {
             //       recordType = qcRecordTypeId;
             //   } else {
                    recordType = standardRecordTypeId;
               // }

                if (!classMap.get(cr.Class__c).isEmpty()) {
                    for (Integer day : classMap.get(cr.Class__c)) {
                        Attendance__c atnd = new Attendance__c();
                        atnd.Status__c = 'Enrolled';
                        atnd.RecordTypeId = recordType;
                        atnd.Class_Roster__c = cr.Id;
                        atnd.Date__c = startOfWeek.addDays(day);
                        atnd.System_Created__c = true;
                        attToInsrt.add(atnd);
                    }
                }
            }
        }
        if (!attToInsrt.isEmpty()) {
            insert attToInsrt;
        }
        
	}

}