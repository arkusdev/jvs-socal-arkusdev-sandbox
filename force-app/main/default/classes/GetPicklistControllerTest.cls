@IsTest
private class GetPicklistControllerTest {
    @IsTest
    static void testBehavior() {
        Test.startTest();
        List<String> picklistValues = GetPicklistController.getPicklistValues('Account', 'Rating');
        Test.stopTest();

        System.assertEquals(3, picklistValues.size());
        System.assert(picklistValues.contains('Hot'));
        System.assert(picklistValues.contains('Warm'));
        System.assert(picklistValues.contains('Cold'));

    }
}