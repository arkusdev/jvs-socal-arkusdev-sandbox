/*
	Use: Util class used from multiple classes
	Description: This the commin Utility class and contains method which are generic in nature.
*/
public class UtilClassForInterviewMails {
	/*This method is used to check given date from the given business hours and return true and false*/
    public static Boolean calBusinessDays(DateTime checkDate, String businesshoursName){
    	//System.debug('>>>>>checkDate'+checkDate);
    	if(checkDate != null && businesshoursName != null){
    		//DateTime dt = DateTime.newInstance(checkDate); // checkDate = 2019, 11, 9
			BusinessHours bhour = [select id from Businesshours where name =: businesshoursName]; // businesshoursName = 'InterviewReminderDates'
			
			if(BusinessHours.isWithin(bhour.id, checkDate)){
				
				//System.debug('>>>>>true');
				return true;
				
			}else{
				//System.debug('>>>>>false');
			    //System.debug('>>>>>nextStartDate'+BusinessHours.nextStartDate(bhour.id, checkDate));
			    return false;
			}
    	}
    	return null;
 	}
}