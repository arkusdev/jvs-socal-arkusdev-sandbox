//**
// Author: Ivanna Kuzemchak
// Date: November 26, 2018
// Description: This is a Schedule Class that invokes BatchAttendanceCreate Class to create Attendance records for active classes.
// It should be scheduled once a week on Sunday. 
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

global class BatchScheduleAttendanceCreate implements Schedulable {
	global void execute(SchedulableContext sc) {
		BatchAttendanceCreate bac = new BatchAttendanceCreate();
		Database.executebatch(bac, 200);

	}
}