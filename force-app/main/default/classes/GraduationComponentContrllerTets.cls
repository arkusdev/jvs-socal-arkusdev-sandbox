//**
// Author: Mazuryk Yaroslav
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
@isTest
public class GraduationComponentContrllerTets {
        @testsetup
    public static void setup(){
        TestFactory.createTestDataForAttendenceController();       
    }
    
    @isTest
    public static void DailyAttendanceController_Test(){
        String datestring = String.valueOf(Date.today());
        Class__c cls = [SELECT id,Name FROM Class__c];
        test.startTest();
        List<Contact> conList = GraduationComponentContrller.getClientRecords(cls.Id,datestring);
        test.stopTest();
        
        system.assertEquals(1, conList.size());
    }
    
        @isTest
    public static void serverUpdateAttendanceRecord_Test(){
        String datestring = String.valueOf(Date.today());
        List<Contact> conList = [SELECT id,Name FROM Contact];
        
        test.startTest();
        GraduationComponentContrller.updateClientStatus(conList);
        test.stopTest();
        
    }

}