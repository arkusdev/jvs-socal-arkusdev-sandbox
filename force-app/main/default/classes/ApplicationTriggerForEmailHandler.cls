/*
    Use : Handller class for Trigger ApplicationTriggerForEmail
    Description: This class is used for sending the Invitation Email Template to the Applicant when ever the disposition is chnaged to values like "Invited 12/6 pm"
                     
*/
public class ApplicationTriggerForEmailHandler {
    public static void sendInvitationEmailTOApplicant(List<Application__c> applicationList,map<Id,Application__c> oldApplicationMap){
        if(applicationList.size() > 0){
            List<EmailTemplate> templateList = new List<EmailTemplate>();
            templateList = [Select Id,body,HtmlValue,Subject from EmailTemplate where name = 'InterviewInvitation' LIMIT 1];
            if(templateList.size() > 0){
                List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
                for(Application__c applicationObj : applicationList){
                    if(applicationObj.Disposition__c != null && oldApplicationMap.containsKey(applicationObj.Id)){
                        if(oldApplicationMap.get(applicationObj.Id).Disposition__c != applicationObj.Disposition__c && applicationObj.Disposition__c.contains('Invited')){
                            String str = applicationObj.Disposition__c;
                            String loc = str.split(' ')[1];
                            String tempDt = str.split(' ')[2];
                            String tempTi = str.split(' ')[3];
                            String timestr = tempTi;
                            if(str.split(' ').size() > 4){
                                String amPm = str.split(' ')[4];
                                timestr = timestr + ' ' + amPm;
                                timestr = timestr.replaceAll('-', '');
                            }
                            String endTime = '';
                            if(str.contains('-')){
                                endTime = str.substringAfter('-');
                            }
                            String month1 = tempDt.split('/')[0];
                            String day1 = tempDt.split('/')[1];
                            String year1 = '';
                            if(tempDt.split('/').size() > 2){
                            	year1 = tempDt.split('/')[2];
                            }
                            if(year1 != null && year1 != '' && month1 != null && month1 != '' && day1 != null && day1 != ''){
                            	DateTime dispositionDate = DateTime.newInstance(integer.valueof(year1), integer.valueof(month1), integer.valueof(day1));
	                            String templateHtmlValue = templateList[0].HtmlValue;
	                            date tempDate = dispositionDate.date();
	                            Integer month = tempDate.month();
	                            Integer day = tempDate.day();
	                            Integer year = tempDate.year();
	                            String dayOfWeek = Datetime.newInstance(year, month, day).format('EEEE');
	                            String monthText = Datetime.newInstance(year, month, day).format('MMMMM');
	                            String daySufix = '';
	                                if(day == 01 || day == 21 || day == 31){daySufix = 'st';}
	                                                else if(day == 02 || day == 22){daySufix = 'nd';}
	                                                    else if(day == 3 || day == 23){daySufix = 'rd';}
	                                                        else if(day == 4 || day >= 5 || day <= 20 || day >= 24 || day <= 30){daySufix = 'th';}
	                            String DateTextFormat= dayOfWeek+', '+ monthText + ' ' + day + daySufix +', ' + String.valueOf(year);
	                            String addressStr = '';
	                            if(loc != null && loc != ''){
	                            	if(InterviewAddresses__c.getValues(loc) != null){
	                            		addressStr = InterviewAddresses__c.getValues(loc).Full_Address__c;
	                            	}
	                            }
	                            templateHtmlValue=templateHtmlValue.replaceAll(Pattern.quote('<![CDATA['), '');
	                            templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWTEXTDATE', DateTextFormat);
	                            templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWDATE', dispositionDate.date().format());
	                            templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWTEXTSTARTTIME', timestr);
	                            templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWTEXTENDTIME', endTime);
	                            templateHtmlValue=templateHtmlValue.replaceAll('INTERVIEWADDRESS', addressStr);
	                            templateHtmlValue=templateHtmlValue.replaceAll('FIRSTNAME', applicationObj.FirstName__c);
	                            templateHtmlValue=templateHtmlValue.replaceAll(']]>', '');
	                            if(applicationObj.Email__c != null && applicationObj.ApplicantId__c != null){
	                                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	                                mail.setTemplateID(templateList[0].Id);
	                                //mail.toAddresses = new String[] { applicationObj.Email__c };
	                                mail.setTargetObjectId(applicationObj.ApplicantId__c);
	                                mail.setHtmlBody(templateHtmlValue);
	                                mail.setSubject(templateList[0].subject);
	                                //mail.setWhatId(applicationObj.Id);
	                                allmsg.add(mail);
	                            }
                            }
                            
                        }
                    }
                }
                Messaging.sendEmail(allmsg,false);
            }
        }
    }
}