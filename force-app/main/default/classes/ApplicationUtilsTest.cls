//**
// Author: Mazuryk Yaroslav
// Date: December 26, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

@isTest
public class ApplicationUtilsTest {

    @testSetup static void setup(){
     TestFactory.createApplicationBaseTestData();
    }


    @isTest
    public static void checkifContactIsCreated_2_Test(){
      List<Contact> conLIst = [SELECT Id, Phone,FirstName,LastName, Email FROM Contact];
      List<Application__c> appListtoCheck = [SELECT id,Email__c,LastName__c,FirstName__c, Cell_Phone__c FROM Application__c]; 

     Test.startTest();
     	Map<String,Id> toAssert = ApplicationUtils.checkifContactIsCreated_2(appListtoCheck);
     Test.stopTest();

     System.assertEquals(1,toAssert.size(), 'Size of list');
     System.assertEquals(conLIst.get(0).Email,appListtoCheck.get(0).Email__c, 'Email');   
    }
    
    
     @isTest
     public static void checkifContactIsCreated_2_Test_Neggative(){
     List<Contact> conLIst = [SELECT Id, Phone,FirstName,LastName, Email FROM Contact];
     List<Application__c> appListtoCheck = [SELECT id,Email__c,LastName__c,FirstName__c, Cell_Phone__c FROM Application__c]; 
     
      for(Contact c : conLIst){
        c.Phone = '+380662323233';
      } 
     update conLIst;    

     Test.startTest();
     Map<String,Id> toAssert = ApplicationUtils.checkifContactIsCreated_2(appListtoCheck);
     Test.stopTest();

     System.assertEquals(0,toAssert.size(), 'Size of list');
     
        
    }

    @isTest
    public static void checkInterviewRecordIsCreated(){
      List<Application__c> applist =  [SELECT id,Name,Email__c,LastName__c,Invite_to_Interview__c,Stage__c,FirstName__c,Applicantid__c, Cell_Phone__c FROM Application__c]; 
      applist.get(0).Stage__c = 'Interview';
      applist.get(0).Invite_to_Interview__c = true;
      update applist;

      ApplicationUtils.createInterviewRecordReletedToApplication(applist);

      List<Interview__c> interList = [SELECT id,Name,Stage__c FROM Interview__c];

      System.assertEquals(1,interList.size(), 'Size of list');
      //System.assertEquals('Phone Interview',interList.get(0).Stage__c, 'Stage of Interview');

    }

}