//**

// Author: Mazuryk Yaroslav
// Date: December 12, 2018
// Description: << Brief description of the code>>
// Revised: <<Upon Revision, add Name of Developer, Date, and brief description of change>>
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org

public class ApplicationTriggerHandler {
        public static void entry(TriggerParams triggerParams) {
        Map <Id, Application__c> newMap  = (Map <Id, Application__c>) triggerParams.newMap;
        Map <Id, Application__c> oldMap  = (Map <Id, Application__c>) triggerParams.oldMap;
        List <Application__c> triggerNew = (List <Application__c>) triggerParams.triggerNew;
        List <Application__c> triggerOld = (List <Application__c>) triggerParams.triggerOld;
        
        if (triggerParams.isBefore) {
            if (triggerParams.isInsert) {
                updateFieldsContactForApp(triggerNew);
                fillAplicantIdIfItsExist(triggerNew);
            }

        }
        if (triggerParams.isAfter) {           
            if (triggerParams.isUpdate) {
                //InsertRecordInterview(triggerNew);

            }
            if(triggerParams.isInsert){
                 createContactForApp(triggerNew);
                 checkApplication(triggerNew);
            }

        }
    }
    
    //update field Stage and update current date
    public static void updateFieldsContactForApp(List<Application__c> triggerNew) {
        for(Application__c app : triggerNew){
            if(app.answer3__c == Constants.GENERAL_YES && app.answer5__c == Constants.GENERAL_YES && app.answer6__c == Constants.GENERAL_YES && app.Duration_of_Work__c > 11){
                app.Stage__c = Constants.STAGE_INTERVIEW;
            }else if(app.answer3__c == Constants.GENERAL_YES || app.answer5__c == Constants.GENERAL_YES || app.answer6__c == Constants.GENERAL_YES){
                 app.Stage__c = Constants.STAGE_REVIEW;
            }
            if(app.Current_Employed__c == true){
                 app.End_date__c = Date.today();
            }
        }        
    }

    public static void fillAplicantIdIfItsExist(List<Application__c> triggerNew){
        Map<String,Id> mapToMatchApplicationWithContact = ApplicationUtils.checkifContactIsCreated_2(triggerNew);            
        
        if(!mapToMatchApplicationWithContact.isEmpty()){
            for(Application__c app : triggerNew){             
                app.ApplicantId__c = mapToMatchApplicationWithContact.get(app.Email__c);                   
            }
        }
            
    }
            

    //  create Contact before inserting Application and connect them
    public static void createContactForApp(List<Application__c> triggerNew) {       
        List<Application__c> toCreateContactList = new List<Application__c>();        
        for(Application__c app : triggerNew){
            if(app.ApplicantId__c == Null || app.ApplicantId__c == null){
                toCreateContactList.add(app);
            }
        }
        //Map<Id,Contact> contactsById
        //Map<Id,Contact> createdContactList =  ApplicationUtils.createnNewContactForApplication(toCreateContactList);   
        Set<Contact> createdContactList =  ApplicationUtils.createnNewContactForApplication(toCreateContactList);           
        List<Application__c> toUpdateAppList = new List<Application__c>();
        // fill in applicant id on Application record
        for(Application__c app :  [SELECT id,Email__c,Cell_Phone__c,ApplicantId__c,FirstName__c FROM Application__c WHERE Id IN: toCreateContactList]){
            //Contact con = createdContactList.get(app.ApplicantId__c);
             /*if(app.Email__c == con.Email && app.Cell_Phone__c == con.Phone){
                 app.ApplicantId__c = con.id;
                 toUpdateAppList.add(app);
               }
*/            
            for(Contact c : createdContactList){
                if(app.Email__c == c.Email && app.Cell_Phone__c == c.Phone && app.FirstName__c == c.FirstName){
                 app.ApplicantId__c = c.id;
                 toUpdateAppList.add(app);
               }
            }
        }
        update toUpdateAppList;
     
        
    }
/*
    public static void InsertRecordInterview(List<Application__c> appList) {

        List<Application__c> aToUpdate = new List<Application__c>();

        for(Application__c app : [SELECT id, Invite_to_Interview__c, Stage__c FROM Application__c WHERE id IN: appList]) {
            if(app.Invite_to_Interview__c == true && app.Stage__c == Constants.STAGE_REVIEW ){
                app.Stage__c = Constants.STAGE_INTERVIEW;
                aToUpdate.add(app);
            }
        }
        update aToUpdate;

        // create interview record 
        ApplicationUtils.createInterviewRecordReletedToApplication(appList);
    }
*/
    ////check when we have to send email notification
    public static void checkApplication(List<Application__c> appList){           
            List<String> toAdress = new List<String>();
            for(Application__c app : appList){
                    if(app.answer6__c == Constants.GENERAL_NO || app.answer3__c == Constants.GENERAL_NO || app.answer5__c == Constants.GENERAL_NO ){
                        toAdress.add(app.Email__c);
                        toAdress.add(app.Secondary_Email__c);
                     //   EmailHelper.sendEmail(Constants.HW_NO_LETTER, app.ApplicantId__c , toAdress);
                    }
                }       
    }
}