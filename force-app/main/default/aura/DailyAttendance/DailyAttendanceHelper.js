/*
*Provisio Partners
* Author : Pulkit Nautiyal
* Description : LEX component helper
* Created Date : March 26th 2018.
*/
({
    getAttendanceRecords: function (component) {
        var action = component.get('c.serverGetAttendanceRecords');
        action.setParams({
            'classId': component.get('v.recordId'),
            'dateString': component.get('v.date')
        });

        action.setCallback(this, function (a) {
            switch (a.getState()) {
                case "SUCCESS":
                    var response = a.getReturnValue();
                    component.set('v.attendanceList', response);
                    break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load attendances"
                    });
                    resultsToast.fire();
                    console.log('Error in calling serverGetAttendanceRecords:\n' +
                        a.getError()[0].message);
                    break;
                default:
                    console.log('Unhandled problem in calling serverGetAttendanceRecords.');
            }
        });
        $A.enqueueAction(action);
    },

    updateAttendanceRecords: function (component) {
        var action = component.get("c.serverUpdateAttendanceRecord");
        action.setParams({
            "AttendanceUpdate": component.get('v.attendanceList')
        });

        action.setCallback(this, function (a) {
               //console.log('value in the get state ' + a.getState());
            if(a.getState() == "SUCCESS")
            {
                console.log('In success ');
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "Success",
                    "title": "Success",
                    "message": "Records were updated!"
                });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
            }
                else if (a.getState() == "Error"){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to update records."
                    });
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    console.log('Error in calling serverUpdateAttendanceRecord:\n' +
                                a.getError()[0].message);
                }
                    else {
                        console.log('Unhandled problem in calling serverUpdateAttendanceRecord.');
                    }
        });
        $A.enqueueAction(action);
    },
    
    SetAllAttendedOnTime: function (component) {
        var templist = component.get("v.attendanceList");
		console.log('Value in Attendance list'+JSON.stringify(templist));
        for(var i=0; i<templist.length; i++)
        {
            console.log('Value in Attendance list'+JSON.stringify(templist[i].Status__c));
         	templist[i].Status__c = 'Present';   
        }
        
        component.set('v.attendanceList', templist);
    },
 
    getToday: function () {
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        var dayDigit = today.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }
        return today.getFullYear() + '-' + monthDigit + '-' + dayDigit;
    },
       SetStatusOnAll: function (component) {
        var templist = component.get("v.attendanceList");
        //Get val of status selected by user on UI
        var selectedUserId = component.find("statusAtt").get("v.value");
        for(var i=0; i<templist.length; i++)
        {
            var status  = JSON.stringify(templist[i].Status__c);
            //Assign that value to Status field for all records
               templist[i].Status__c = selectedUserId;  
             //'Attended On Time'
         }
        component.set('v.attendanceList', templist);

    },
});