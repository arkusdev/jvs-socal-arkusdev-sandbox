/*
*Provisio Partners
* Author : Yaroslav Mazuryk
* Description : LEX component controller
* Created Date : March 26th 2018.
*/
({
    // doInit method
    doInit: function (component, event, helper) {
        component.set('v.date', new Date().toISOString());
        helper.getAttendanceRecords(component);
    },

    //Method will be called after date selection
    getAttendanceRecords : function(component, event, helper) {
        helper.getAttendanceRecords(component)
    },
    
    //method will be called after select All button clicked
    SelectAttendedOnTime : function(component, event, helper) {
        helper.SetAllAttendedOnTime(component);
    },

    //Method called when submit for saving record
    handleClick : function(component, event, helper) {
        helper.updateAttendanceRecords(component,event,helper);
    },
	
    //Method called on cancel button clicked
    closeWindow : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    SelectStatusOnAll : function(component, event, helper) {
       helper.SetStatusOnAll(component);
    }
});