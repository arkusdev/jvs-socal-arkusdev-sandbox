/*
*Provisio Partners
* Author : Yaroslav Mazuryk
* Description : LEX component controller
* Created Date : March 26th 2018.
*/
({
    // doInit method
    doInit: function (component, event, helper) {
        component.set('v.date', new Date().toISOString());
        helper.getClientsRecord(component);
        helper.setSpace(component);
    },

    //Method called when submit for saving record
    handleClick : function(component, event, helper) {
        helper.updateClientRecords(component,event,helper);
    },
	
    //Method called on cancel button clicked
    closeWindow : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    SelectStatusOnAll : function(component, event, helper) {
       helper.SetStatusOnAll(component);
    }
});