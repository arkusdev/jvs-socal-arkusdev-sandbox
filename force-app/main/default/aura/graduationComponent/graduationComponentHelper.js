/*
*Provisio Partners
* Author : Yaroslav Mazuryk
* Description : LEX component helper
* Created Date : March 26th 2018.
*/
({
    getClientsRecord: function (component) {
        var action = component.get('c.getClientRecords');
        action.setParams({
            'classId': component.get('v.recordId'),
            'dateString': component.get('v.date')
        });

        action.setCallback(this, function (a) {
            switch (a.getState()) {
                case "SUCCESS":
                    var response = a.getReturnValue();
                    component.set('v.contactList', response);
                    break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load attendances"
                    });
                    resultsToast.fire();
                    console.log('Error in calling getClientRecords:\n' +
                        a.getError()[0].message);
                    break;
                default:
                    console.log('Unhandled problem in calling getClientRecords.');
            }
        });
        $A.enqueueAction(action);
    },

    updateClientRecords: function (component) {
        var action = component.get("c.updateClientStatus");
        action.setParams({
            "conList": component.get('v.contactList'),
            'classId': component.get('v.recordId')
        });

        action.setCallback(this, function (a) {
               //console.log('value in the get state ' + a.getState());
            if(a.getState() == "SUCCESS")
            {
                console.log('In success ');
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "Success",
                    "title": "Success",
                    "message": "Records were updated!"
                });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
            }
                else if (a.getState() == "Error"){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to update records."
                    });
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    console.log('Error in calling serverUpdateAttendanceRecord:\n' +
                                a.getError()[0].message);
                }
                    else {
                        console.log('Unhandled problem in calling serverUpdateAttendanceRecord.');
                    }
        });
        $A.enqueueAction(action);
    },

       SetStatusOnAll: function (component) {
        var templist = component.get("v.contactList");
        //Get val of status selected by user on UI
        var selectedUserId = component.find("statusAtt").get("v.value");
        for(var i=0; i<templist.length; i++)
        {
            var status  = JSON.stringify(templist[i].Status__c);
            //Assign that value to Status field for all records
               templist[i].Status__c = selectedUserId;  
             //'Attended On Time'
         }
        component.set('v.contactList', templist);

    },
    // set space to aura iterable 
      setSpace: function (component) {
        var templist = component.get("v.space");
          var space = '. ';
        component.set('v.space', space);

    },
});