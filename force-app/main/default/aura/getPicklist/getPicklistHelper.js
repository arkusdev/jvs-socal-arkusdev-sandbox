({
 fetchPickListVal: function(component) {
        var action = component.get("c.getPicklistValues");
        action.setParams({
            "objectName": component.get("v.objectName"),
            "fieldName": component.get("v.fieldName")
        });

        var picklistValues = [];
        action.setCallback(this, function(response) {
            if (response.getState() !== "SUCCESS") {
                console.log("Failed to load picklist values." +
                    "please ensure you have the objectName and " +
                    "fieldName attributes assigned on the picklist.");
                return;
            }
            picklistValues.push({
                text: "--- None Selected ---",
                value: null
            })
            var responseValues = response.getReturnValue();
            for (var i = 0; i < responseValues.length; i++) {
                picklistValues.push({
                    text: responseValues[i],
                    value: responseValues[i]
                });
            }

            component.set("v.picklistOptions", picklistValues);
        });
        $A.enqueueAction(action);
    }
})