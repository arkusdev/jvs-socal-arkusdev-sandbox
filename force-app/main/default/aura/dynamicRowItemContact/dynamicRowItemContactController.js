({
    AddNewRow : function(component, event, helper){
        // fire the AddNewRow Lightning Event
        component.getEvent("AddRowEvt").fire();
    },

    removeRow : function(component, event, helper){
        // fire the DeleteRowEvent Lightning Event and pass the deleted Row Index to Event parameter/attribute
        component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    }

})